local announcement_dialog = ...

local vars = DroidButtons.vars
local ui = DroidButtons.ui
local db = DroidButtons

local bool_map  = {[true] = 'ON', [false] = 'OFF'}
local state_map = {ON = true,  OFF = false}
local val_map   = {[true] = 1, [false] = 0}

local list2, list3 = dofile("templates.lua")

local region_type_radio_list = {'button', 'field'}
for i, v in ipairs(region_type_radio_list) do region_type_radio_list[v] = i end

local button_type_radio_list = {'standard', 'configurable'}
for i, v in ipairs(button_type_radio_list) do button_type_radio_list[v] = i end

local function px(i) return tonumber(HUDSize(i/100, 0):match("(%d+)x")) end
local function py(i) return tonumber(HUDSize(0, i/100):match("x(%d+)")) end

local function check_number(i, min, max)
	i = tonumber(i); min = tonumber(min); max = tonumber(max)
	if     (not i)           then return false 
	elseif (min and i < min) then return false
	elseif (max and i > max) then return false
	end
	return true
end

local function ispluscommand(str) 
	assert(type(str) == "string", "bad argument #1 to 'ispluscommand' (string expected, got "..type(str)..")")
	if (str:sub(1,1) == "+") then return true end
	return false
end

local function make_dirty(ctl, num)
	if (num) then 
		local result = check_number(num, 0)
		if (result) then ctl.fgcolor = "255 255 255"; vars.TabState.BadValue = false
		else             ctl.fgcolor = "255 0 0"; vars.TabState.BadValue = true
		end
	end
	ui.Tabs:MakeDirty()
end


local function create_subdlg(ctrl)
	local dlg = iup.dialog{ctrl, border="NO", menubox="NO", resize="NO", bgcolor="0 0 0 0 *", expand = "YES", } --size = "FULL"}
	dlg.visible = "YES"
	return dlg
end

local function create_scrollbox(content)
	local scrollbox = iup.stationsublist{{}, control = "YES", expand = "YES",}
	iup.Append(scrollbox, create_subdlg(content))
	scrollbox[1] = 1
	return scrollbox
end

-- -------------------------------------------------------------------
-- Touch-to-Place Interface
-- -------------------------------------------------------------------
local function show_placement_interface(buttonindex, title, rtype, xpos, ypos, width, height, font)
	-- only xpos and ypos are control references, the rest should be string/number values
	local buttondef, modified
	
	local save, save_region, save_func
	local cancel, cancel_region, cancel_func
	local place_region, place_func

	-- Odd region z-order behavior allows activating multiple regions in a stack with multi-touch.
	-- Rather than destroying all regions when touch-to-place is activated, I'm just adding another region
	-- directly below the place_region to prevent the most common form of multi-touch.
	local double_touch_preventer
	
	local button_size = string.format("%dx%d", db.defaults["button_width"], db.defaults["button_height"])
	save = iup.stationbutton{title = 'Save', size = button_size, font = db.defaults["font"], cx = 8, cy = 8}
	cancel = iup.stationbutton{title = 'Cancel', size = button_size, font = db.defaults["font"], cx = tonumber(save.cx) + db.defaults["button_width"] + 8, cy = 8}

	-- Layout ---------------------------------------------------------
	iup.Append(iup.GetNextChild(db.cbox), save)
	iup.Append(iup.GetNextChild(db.cbox), cancel)
	iup.Refresh(db.cbox)
	
	-- ----------------------------------------------------------------
	
	function place_func(pW, pH)
		buttondef.x = math.floor(pW * db.xres)
		buttondef.y = math.floor(pH * db.yres)
		for axis, leg in pairs({x = 'w', y = 'h'}) do
			-- adjust axis by half the leg to center the button
			buttondef[axis] = buttondef[axis] - math.floor(buttondef[leg]/2)
			-- bring axis back within the screen if necessary
			if (buttondef[axis] < 0) then 
				buttondef[axis] = 0
			elseif (buttondef[axis] + buttondef[leg] - 1 > db[axis..'res']) then 
				buttondef[axis] = db[axis..'res'] - buttondef[leg] + 1
			end
			-- snap to grid if applicable.  I chose to always snap towards 0.
			if (db.snap_to_grid) then
				local r = buttondef[axis] % db.grid_size
				buttondef[axis] = buttondef[axis] - r
			end
		end
		modified = true
		db:UpdateButton(buttonindex, buttondef, true, false)
	end

	function cancel_func()
		HUD.help_text.title = ''

		iup.Detach(save); iup.Destroy(save); save = nil
		iup.Detach(cancel); iup.Destroy(cancel); cancel = nil

		local cleanup = Timer()
		cleanup:SetTimeout(1, function() 
			gkinterface.DestroyTouchRegion(cancel_region)
			db.regions[cancel_region] = nil
			gkinterface.DestroyTouchRegion(save_region)
			db.regions[save_region] = nil
			gkinterface.DestroyTouchRegion(place_region)
			db.regions[place_region] = nil
			gkinterface.DestroyTouchRegion(double_touch_preventer)
		end)

		db:UpdateButton(buttonindex, nil, true, false)
		ShowDialog(ui.MainDialog)
	end
	
	function save_func()
		if (modified) then 
			xpos.value = buttondef.x
			ypos.value = buttondef.y
			make_dirty()
		end
		cancel_func()
	end
	
	cancel.action = cancel_func
	save.action = save_func
	
	-- ----------------------------------------------------------------

	buttondef = {
		title = title,
		type = rtype,
		x = xpos.value,
		y = ypos.value,
		w = width,
		h = height,
		font = font,
	}
	db:UpdateButton(buttonindex, buttondef, true, false)
	cancel_region = gkinterface.CreateTouchRegion(nil, nil, nil, false, false, false, false, false, false, tonumber(cancel.cx), tonumber(cancel.cy), tonumber(cancel.cx) + db.defaults["button_width"] - 1, tonumber(cancel.cy) + db.defaults["button_height"] - 1, -100)
	save_region = gkinterface.CreateTouchRegion(nil, nil, nil, false, false, false, false, false, false, tonumber(save.cx), tonumber(save.cy), tonumber(save.cx) + db.defaults["button_width"] - 1, tonumber(save.cy) + db.defaults["button_height"] - 1, -100)
	place_region = gkinterface.CreateTouchRegion(nil, nil, nil, false, false, false, false, false, false, 0, 0, db.xres, db.yres, -99)
	double_touch_preventer = gkinterface.CreateTouchRegion(nil, nil, nil, false, false, false, false, false, false, 0, 0, db.xres, db.yres, -98)

	db.regions[cancel_region] = cancel_func
	db.regions[save_region] = save_func
	db.regions[place_region] = place_func

	HUD.help_text.title = 'Touch the screen to place the button.'
	
	HideDialog(ui.MainDialog)
end

-- -------------------------------------------------------------------
-- Edit Button Controls
-- -------------------------------------------------------------------
function ui:BuildButtonControls()
	local bc = {}
	local commands_zbox, commands_zbox_items
	local button_type_radio_items
	local standard_commands, configurable_commands
	local add_button
	
	-- Button region controls.  They will need to be populated later.
	bc.cmd1, bc.cmd1text, bc.cmd1list = list2{list = vars.commandlist, visible_items = 10, action = make_dirty}
	bc.cmd2, bc.cmd2text, bc.cmd2list = list2{list = vars.commandlist, visible_items = 10}
	bc.cmd2c, bc.cmd2ctext, bc.cmd2clist = list2{list = vars.commandlist, visible_items = 10, action = make_dirty, bgcolor = '55 122 150 255 *', bginactive = "128 128 128 255 *", active = "NO"}
	
	
	button_type_radio_items = {iup.stationradio{title = 'Standard', value = "ON"}, iup.stationradio{title = 'Configurable'}, gap = "%2"}
	bc.button_type_radio = iup.radio{iup.hbox(button_type_radio_items)}

	standard_commands = iup.vbox{
		iup.vbox{
			iup.label{title = "Button Command(s)"},
			iup.hbox{iup.label{title = 'Short-press: '}, iup.fill{}, bc.cmd1},
		},
		iup.hbox{iup.label{title = 'Long-press: '}, iup.fill{}, bc.cmd2},
		iup.hbox{iup.label{title = 'Long-press cancel: '}, iup.fill{}, bc.cmd2c},
		gap = "%3",
		expand = "NO",
	}
	
	bc.configurable_list = list3{expand = "YES", scrollbar = "VERTICAL"}--size = "500x500"}
	add_button = iup.stationbutton{title = "Add"}
	
	configurable_commands = iup.vbox{
		bc.configurable_list,
		iup.hbox{
			iup.fill{},
			add_button,
			iup.fill{},
		},
	}
	
	-- Layout ---------------------------------------------------------
	commands_zbox_items = {alignment = "NW"}
	-- Button region controls
	commands_zbox_items[1] = standard_commands
	-- Field region controls
	commands_zbox_items[2] = configurable_commands
	commands_zbox = iup.zbox(commands_zbox_items)


	bc.layout = iup.vbox{
		bc.button_type_radio,
		commands_zbox,
		gap = "%3",
	}

	-- Callbacks & Functions -----------------------------------------

	function add_button:action()
		local list = bc.configurable_list
		list:AddItem{title = "", command = ""}
		make_dirty()
		list:PopulateList()
		if (list:GetNumItems() == 5) then self.active = "NO" end
	end

	bc.cmd2:SetActionFunc(function(self, c, after) 
		local result = ispluscommand(after)
		bc.cmd2c:Activate(result)
		if (not result) then bc.cmd2c:Clear() end
		make_dirty()
	end)

	function bc.button_type_radio:set_value(v)
		self.value = button_type_radio_items[v]
		button_type_radio_items[v]:action()
	end

	local function radio_switch(index)
		commands_zbox.value = commands_zbox_items[index]
	end
	
	button_type_radio_items[1].action = function(self, v) 
		if (v) then make_dirty() end
		radio_switch(1)
	end
		
	button_type_radio_items[2].action = function(self, v)
		if (v) then make_dirty() end
		radio_switch(2)
	end
	
	bc.configurable_list:SetCreateItemFunc(function(item, index)
		local delete, move_up, move_down, title, cmd, layout
		move_size = "%3x%3"
		move_label_size = "%4x%4"
		delete = iup.stationbutton{title = "X", size = "%3x%5"}
		move_up = iup.stationbutton{title = "Up", size = move_size, fgcolor = "0 0 0 0 *", bgcolor = "0 0 0 0 *"}
		move_down = iup.stationbutton{title = "Down", size = move_size, fgcolor = "0 0 0 0 *", bgcolor = "0 0 0 0 *"}

		cmd = list2{name = "command", list = vars.commandlist_noplus, visible_items = 10, action = make_dirty}
		cmd.name = "command"
		cmd:SetText(item.command)
		title = iup.text{name = "title", value = item.title, size = cmd:GetWidth()}

		layout = iup.hbox{
			iup.vbox{
				iup.hbox{iup.label{title = "Title:"}, iup.fill{}, title, gap = 5},
				iup.hbox{iup.label{title = "Command:"}, iup.fill{}, cmd, gap = 5},
				gap = "%1",
				expand = "NO"
			},
			iup.vbox{
				iup.zbox{
					move_up,
					iup.label{title = "Up", image = IMAGE_DIR.."arrow.png", size = move_label_size},
					alignment = "ACENTER",
					all = "YES",
				},
				iup.fill{},
				iup.zbox{
					move_down,
					iup.label{title = "Down", image = IMAGE_DIR.."arrow_down.png", size = move_label_size},
					alignment = "ACENTER",
					all = "YES",
				},
			},
			iup.vbox{
				iup.fill{},
				delete,
				iup.fill{},
			},
			gap = 10,
			margin = "10x10",
		}

		function delete:action()
			bc.configurable_list:RemoveItem(index)
			add_button.active = "YES"
			make_dirty()
		end

		function move_up:action()
			bc.configurable_list:MoveItemUp(index)
			make_dirty()
		end
	                                      
		function move_down:action()
			bc.configurable_list:MoveItemDown(index)
			make_dirty()
		end
	                                      
		cmd:SetActionFunc(function(self, c, after)
			if (ispluscommand(after)) then return iup.IGNORE end
			iup.GetDialog(self):UpdateItem()
			make_dirty()
		end)
							

		function title:action(c, after)
			iup.GetDialog(self):UpdateItem()
			make_dirty()
		end

		return layout
	end)
	bc.configurable_list:SetItems{{title = "", command = ""}}
	bc.configurable_list:SetColors{'30 55 78 96', '42 74 96 96'} --, [0] = '65 100 127 255'}
	bc.configurable_list:PopulateList()
	
		
	return bc
end	


-- -------------------------------------------------------------------
-- Edit Field Controls
-- -------------------------------------------------------------------
function ui:BuildFieldControls()
	local fc = {fbc = {}, sensitivity= {}}
	-- Field region controls
	fc.xaxiscmd = iup.stationsublist(vars.joycmdlist)
	fc.yaxiscmd = iup.stationsublist(vars.joycmdlist)
	fc.fbc.invertX = iup.stationtoggle{title = 'Invert X-axis'}
	fc.fbc.invertY = iup.stationtoggle{title = 'Invert Y-axis'}
	fc.fbc.mouselookX = iup.stationtoggle{title = 'Delta X'}
	fc.fbc.mouselookY = iup.stationtoggle{title = 'Delta Y'}
	fc.fbc.relative = iup.stationtoggle{title = 'Relative'}
	fc.fbc.lookspring = iup.stationtoggle{title = "Turret Lookspring"}
	fc.fbc.trackingcircles = iup.stationtoggle{title = "Touch Tracking Circles"}
	fc.sensitivity.curve = iup.stationsublist(vars.sensitivity_curve_list)
	fc.sensitivity.filter = iup.stationtoggle{title = 'Filtering'}
	fc.sensitivity.display = iup.label{title = "0.00x", alignment = 'ARIGHT'}
	fc.sensitivity.value = iup.canvas{size = "200x"..Font.Default, scrollbar = "HORIZONTAL", xmin = 1, xmax = 300, posx = 100, dx = Font.Default/2, border = "NO"}

	fc.layout = iup.vbox{
		iup.hbox{
			iup.vbox{
				iup.hbox{iup.label{title = "X-axis"}, iup.fill{}},
				iup.pdasubframebg{fc.xaxiscmd},
				fc.fbc.invertX,
				fc.fbc.mouselookX,
				gap = "%1"
			},
			iup.vbox{
				iup.hbox{iup.label{title = "Y-axis"}, iup.fill{}},
				iup.pdasubframebg{fc.yaxiscmd},
				fc.fbc.invertY,
				fc.fbc.mouselookY,
				gap = "%1"
			},
			gap = "%10",
		},
		iup.hbox{iup.fill{}, fc.fbc.relative, fc.fbc.lookspring, iup.fill{}, gap = "%5" },
		iup.vbox{
			iup.label{title = "Sensitivity"},
			iup.hbox{iup.label{title = "Curve"}, iup.pdasubframebg{fc.sensitivity.curve}, fc.sensitivity.filter, gap = "%3"},
			iup.hbox{fc.sensitivity.display, fc.sensitivity.value, gap = "%1"},
			gap = "%1",
		},
		iup.hbox{fc.fbc.trackingcircles, iup.fill{}},
		gap = "%3",
		expand = "NO",
	}
	
	function fc.xaxiscmd:action(t, i, v)
		if (i == tonumber(self.cursel)) then return iup.IGNORE end
		if (v == 1) then self.cursel = i; make_dirty() end
	end
	function fc.yaxiscmd:action(t, i, v)
		if (i == tonumber(self.cursel)) then return iup.IGNORE end
		if (v == 1) then self.cursel = i; make_dirty() end
	end

	for _, c in pairs(fc.fbc) do 
		function c:action(v) make_dirty() end 
	end

	function fc.sensitivity.curve:action(t, i, v)
		if (i == tonumber(self.cursel)) then return iup.IGNORE end
		if (v == 1) then self.cursel = i; make_dirty() end
	end
	function fc.sensitivity.filter:action(v) make_dirty() end

	function fc.sensitivity.value:scroll_cb(op, posx, posy) 
		-- called a PopulateFields() with op == nil to set display
		fc.sensitivity.display.title = string.format("%4.2fx", posx / 100)
		if (op) then make_dirty() end
	end

	return fc
end

-- -------------------------------------------------------------------
-- Edit Common Controls
-- -------------------------------------------------------------------
function ui:BuildCommonControls()
	local cc = {}
	-- Common region controls
	cc.place = iup.stationbutton{title = 'Touch-to-Place'}
	cc.xpos = iup.text{size = px(7.5)}
	cc.ypos = iup.text{size = px(7.5)}
	cc.width = iup.text{size = px(7.5)}
	cc.height = iup.text{size = px(7.5)}
	cc.font = iup.text{size = px(7.5)}
	cc.isvisible = iup.stationtoggle{title = '', size = px(7.5)}

	cc.layout = iup.vbox{
		iup.hbox{iup.fill{}, cc.place, iup.fill{}},
		iup.hbox{
			iup.vbox{
				iup.hbox{iup.label{title = 'X Position: '}, iup.fill{}, cc.xpos,},
				iup.hbox{iup.label{title = 'Width: '}, iup.fill{}, cc.width,},
				iup.hbox{iup.label{title = 'Font Size: '}, iup.fill{}, cc.font,},
				gap = "%3"
			},
			iup.vbox{
				iup.hbox{iup.label{title = 'Y Position: '}, iup.fill{}, cc.ypos},
				iup.hbox{iup.label{title = 'Height: '}, iup.fill{}, cc.height},
				iup.hbox{iup.label{title = 'Visible '}, iup.fill{}, cc.isvisible},
				gap = "%3"
			},
			gap = "%3",
		},
		iup.fill{},
		expand = "VERTICAL",
		gap = "%3"
	}

	
	-- cc.place:action() created in parent function due to reliance on additional controls
	do	
		local function func(self, c, after)
			if (c == 27 and after == "") then return end
			make_dirty(self, after)
		end
		cc.xpos.action = func
		cc.ypos.action = func
		cc.width.action = func
		cc.height.action = func
		cc.font.action = func
	end
	function cc.isvisible:action(v) make_dirty() end
	
	return cc
end

-- -------------------------------------------------------------------
-- Edit Region Interface
-- -------------------------------------------------------------------
function ui:BuildEditRegionInterface()
	local buttonindex -- set by PopulateFields() below
	local newbutton -- set by PopulateFields() below
	
	local editcontrols, editcontrols_scrollbox, editcontrols_content
	local editcontrols_zbox, editcontrols_zbox_items

	local bc, fc, cc -- table to hold button/field/common controls
	
	local title, type_radio, type_radio_items

	bc = ui:BuildButtonControls()
	fc = ui:BuildFieldControls()
	cc = ui:BuildCommonControls()

	title = iup.text{size = bc.cmd1text.size}
	
	type_radio_items = {iup.stationradio{title = 'Button', value = "ON"}, iup.stationradio{title = 'Field'}, gap = "%2"}
	type_radio = iup.radio{iup.hbox(type_radio_items)}
	
	-- Layout ---------------------------------------------------------
	editcontrols_zbox_items = {alignment = "NW"}
	-- Button region controls
	editcontrols_zbox_items[1] = bc.layout
	-- Field region controls
	editcontrols_zbox_items[2] = fc.layout
	editcontrols_zbox = iup.zbox(editcontrols_zbox_items)

	editcontrols_content = iup.vbox{
		iup.pdasubframebg{iup.hbox{iup.label{title = "Region Type: "}, type_radio, iup.fill{expand = "YES"}}},
		iup.vbox{
			iup.vbox{
				iup.hbox{iup.label{title = 'Title: '}, title},
				editcontrols_zbox,
				iup.fill{},
				gap = "%3"
			},
			cc.layout,
			gap = "%2",
			margin = "%1x%1"
		},
		gap = "%1",
	}
	editcontrols_content[1].border = string.format("%d %d %d %d", px(1), py(1), px(1), py(1)) -- left, top, right, bottom
	editcontrols_content[2].border = string.format("%d %d %d %d", px(1), py(1), px(1), py(1)) -- left, top, right, bottom

	editcontrols_scrollbox = create_scrollbox(editcontrols_content)
	editcontrols_scrollbox.active = "NO"
	editcontrols_scrollbox.bgcolor = "0 0 0 0 *"

	editcontrols = iup.pdasubsubsubframe2{
		iup.pdasubsubframebg{
			editcontrols_scrollbox,
			border = string.format("%d %d %d %d", px(0.5), py(1), px(0.5), py(1)) -- left, top, right, bottom
		},
	}
	-- ----------------------------------------------------------------

	local function reset_fields()
		cc.xpos.fgcolor = "255 255 255"
		cc.ypos.fgcolor = "255 255 255"
		cc.width.fgcolor = "255 255 255"
		cc.height.fgcolor = "255 255 255"
		cc.font.fgcolor = "255 255 255"

		vars.TabState.BadValue = false
	end
	
	function title:action(c, after) make_dirty() end

	function cc.place:action()
		if (vars.TabState.BadValue) then
			PopupDialog(announcement_dialog, iup.CENTER, iup.CENTER)
			return iup.IGNORE
		end
		show_placement_interface(buttonindex, title.value, type_radio.value.title:lower(), cc.xpos, cc.ypos, tonumber(cc.width.value), tonumber(cc.height.value), tonumber(cc.font.value))
	end
		
		
		
		

	function type_radio:set_value(v)
		self.value = type_radio_items[v]
		type_radio_items[v]:action()
	end

	local function radio_switch(index)
		editcontrols_zbox.value = editcontrols_zbox_items[index]
		if (newbutton) then
			cc.width.value = db.defaults[region_type_radio_list[index].."_width"]
			cc.height.value = db.defaults[region_type_radio_list[index].."_height"]
			if (buttonindex ~= 0) then
				local buttondef = db.buttondefs[buttonindex]
				buttondef.type = region_type_radio_list[index]
				buttondef.w = cc.width.value
				buttondef.h = cc.height.value
				db:DeleteButton(buttonindex)
				db:CreateButton(buttonindex)
			end
		end
	end
	
	type_radio_items[1].action = function(self, v) radio_switch(1) end
		
	type_radio_items[2].action = function(self, v) radio_switch(2) end

	function editcontrols:PopulateFields(index)
		buttonindex = index
		local default = db.defaults
		local buttondef = db.buttondefs[index]

		if (buttondef) then
			newbutton = false
		else
			buttondef = {
				type = "button", 
				x = 0, 
				y = 0, 
				w = default["button_width"], 
				h = default["button_height"], 
				font = default["font"]
			}
			reset_fields()
			if (index ~= 0) then
				db.buttondefs[index] = buttondef
				db:CreateButton(index)
			end
			newbutton = true
		end
 
		-- Common region controls
		title.value = buttondef.title or ""
		cc.xpos.value = buttondef.x or 0
		cc.ypos.value = buttondef.y or 0
		cc.width.value = buttondef.w or default[buttondef.type.."_width"]
		cc.height.value = buttondef.h or default[buttondef.type.."_height"]
		cc.font.value = buttondef.font or default["font"]
		cc.isvisible.value = bool_map[buttondef.visible or true]

		-- If it's a newbutton or a reset (index == 0) then all fields should be cleared
		-- If it's an existing button, then it shouldn't matter if the other region types controls are populated
		-- Button region controls
		bc.cmd1text.value = buttondef.command1 or ''
		bc.cmd2text.value = buttondef.command2 or ''
		bc.cmd2ctext.value = buttondef.command2cancel or ''
		bc.cmd2c:Activate(buttondef.command2cancel)
		bc.button_type_radio:set_value(button_type_radio_list[buttondef.button_type or "standard"]) --TODO update data structure or leave default?
		bc.configurable_list:SetItems(buttondef.commands or {})
		bc.configurable_list:PopulateList()

		-- Field region controls
		fc.xaxiscmd.value = vars.joycmdlist2[buttondef.command1] or vars.joycmdlist2['NONE']
		fc.xaxiscmd.cursel = fc.xaxiscmd.value
		fc.yaxiscmd.value = vars.joycmdlist2[buttondef.command2] or vars.joycmdlist2['NONE']
		fc.yaxiscmd.cursel = fc.yaxiscmd.value
		fc.fbc.invertX.value = bool_map[buttondef.invertX or false]
		fc.fbc.invertY.value = bool_map[buttondef.invertY or false]
		fc.fbc.mouselookX.value = bool_map[buttondef.mouselookX or false]
		fc.fbc.mouselookY.value = bool_map[buttondef.mouselookY or false]
		fc.fbc.relative.value = bool_map[buttondef.relative or false]
		fc.fbc.lookspring.value = bool_map[not(buttondef.lookspring) or false]
		fc.fbc.trackingcircles.value = bool_map[buttondef.trackingcircles or true]

		local cv = buttondef.Scurve or 256 -- default to unfiltered linear curve
		fc.sensitivity.filter.value = bool_map[(cv < 256) or false]
		cv = cv + 1 - (cv < 256 and 0 or 256)
		fc.sensitivity.curve.value = cv
		fc.sensitivity.curve.cursel = cv
		fc.sensitivity.value.posx = buttondef.Svalue or 100
		fc.sensitivity.value:scroll_cb(nil, fc.sensitivity.value.posx)

		type_radio:set_value(region_type_radio_list[buttondef.type])
		local type_idx = region_type_radio_list[buttondef.type]
		for i, ctl in ipairs(type_radio_items) do
			if (type_idx == i or newbutton) then ctl.active = "YES" else ctl.active = "NO" end
		end

		-- index == 0 designates a deleted record.  Disable all controls.
		editcontrols_scrollbox.active = index == 0 and "NO" or "YES"
		editcontrols_scrollbox.bgcolor = index == 0 and "0 0 0 0 *" or "255 255 255 255 *"
		editcontrols_scrollbox.scroll = "TOP"
	end
	
	function editcontrols:Save()
		if (vars.TabState.BadValue) then return false end
		local recreate_region
		local buttondef = db.buttondefs[buttonindex]
		local newtitle = buttondef.title ~= title.value

		local function store_value(k, v, keep_region)
			if (buttondef[k] ~= v and not keep_region) then recreate_region = true end
			buttondef[k] = v
		end

		store_value('type', type_radio.value.title:lower(), true)
		store_value('title', title.value or '', true)
		store_value('x', tonumber(cc.xpos.value), true)
		store_value('y', tonumber(cc.ypos.value), true)
		store_value('w', tonumber(cc.width.value), true)
		store_value('h', tonumber(cc.height.value), true)
		store_value('font', tonumber(cc.font.value), true)
		store_value('visible', state_map[cc.isvisible.value], true)

		if (buttondef.type == 'button') then
			--local ipc = ispluscommand(buttondef.command1 or "")
			--if ((ipc and cmd1text.value ~= buttondef.command1) or (not ipc and ispluscommand(cmd1text.value))) then recreate_region = true end

			store_value('button_type', bc.button_type_radio.value.title:lower(), false)
			if (buttondef.button_type == 'standard') then
				store_value('command1', bc.cmd1text.value ~= '' and bc.cmd1text.value or nil, not (ispluscommand(buttondef.command1 or "") or ispluscommand(bc.cmd1text.value)))
				store_value('command2', bc.cmd2text.value ~= '' and bc.cmd2text.value or nil, true)
				store_value('command2cancel', bc.cmd2ctext.value ~= '' and bc.cmd2ctext.value or nil, true)
				store_value('commands', nil, true)
				
			elseif (buttondef.button_type == 'configurable') then
				store_value('command1', nil, not (ispluscommand(buttondef.command1 or "")))
				store_value('command2', nil, true)
				store_value('command2cancel', nil, true)
				local button_items = bc.configurable_list:GetItems()
				store_value('commands', button_items, true)
				buttondef.commands.current = #button_items > 0 and 1 or nil -- always reset the current command in case of changes/deletions
			end

		elseif (buttondef.type == 'field') then
			store_value('command1', vars.joycmdlist3[tonumber(fc.xaxiscmd.value)] ~= "NONE" and vars.joycmdlist3[tonumber(fc.xaxiscmd.value)] or nil)
			store_value('command2', vars.joycmdlist3[tonumber(fc.yaxiscmd.value)] ~= "NONE" and vars.joycmdlist3[tonumber(fc.yaxiscmd.value)] or nil)

			for p, c in pairs(fc.fbc) do
				if (p == 'lookspring') then -- TODO If more exceptions are needed, consider making a table for each one with a true/false value to modify the result
					store_value(p, not(state_map[c.value]))
				else
					store_value(p, state_map[c.value])
				end
			end
			
			store_value('Scurve', tonumber(fc.sensitivity.curve.value) - 1 + (state_map[fc.sensitivity.filter.value] and 0 or 1)*256)
			store_value('Svalue', tonumber(fc.sensitivity.value.posx))
		end
		
		db:save_buttondefs()
		if (recreate_region) then
			db:DeleteButton(buttonindex, true) -- calling with teardown to prevent the position from being vacated in a lower index button before making a change in a higher indexed button
			db:CreateButton(buttonindex)
		else
			db:UpdateButton(buttonindex)
		end

		if (newbutton or newtitle) then
			return function() ui.ButtonsTab:PopulateTab(buttonindex) end
		end

		return true
	end

	function editcontrols:Undo()
		reset_fields()
		if (newbutton) then
			-- Treat undoing a newbutton as a delete
			db:DeleteButton(buttonindex)
			table.remove(db.buttondefs, buttonindex)
			return function() ui.ButtonsTab:PopulateTab(0) end
		else
			db:UpdateButton(buttonindex, nil, true, false)
			self:PopulateFields(buttonindex)
		end
	end
	-- ----------------------------------------------------------------

	return editcontrols
end
