local oldPopupDialog = PopupDialog
function PopupDialog(dlg, x, y)
	oldPopupDialog(dlg, x, y)
	local retval = dlg.RetVal and dlg.RetVal[1] -- using a table is the only way to preserve datatype in an IUP control
	dlg.RetVal = nil
	return retval
end

declare("LoadSystemNotes2", function(id, character)
	assert(id, "wrong number of arguments to 'LoadSystemNotes2'")
	assert(type(id) == "number", "bad argument #1 to 'LoadSystemNotes2' (number expected, got "..type(id)..")")
	if (character) then
		assert(type(character) == "string", "bad argument #2 to 'LoadSystemNotes2' (string expected, got "..type(character)..")")
	end

	local function loadnote(id, character)
		local datafunc, err = loadfile("settings/"..character.."/system"..tostring(id).."notes.txt")
		if (datafunc) then
			local success, data = pcall(datafunc)
			if (success and type(data) == "table" and data.data) then
				return data.data, data.timestamp
			end
		end
	end

	local data, timestamp, latest_data
	local latest_timestamp = 0
	-- if character is provided, then attempt to get the requested data first
	if (character) then
		data = loadnote(id, character)
		if (data) then return data, true end
	end
	
	-- if no data or character is missing, scan all characters on the account for the latest data
	for i = 1, 6 do
		character = GetCharacterInfo(i)
		if (character) then
			data, timestamp = loadnote(id, character)
			if (data and timestamp > latest_timestamp) then
				latest_data, latest_timestamp = data, timestamp
			end
		end
	end
	if (latest_data) then return latest_data, true end
	return ""
end)

declare("SaveSystemNotes2", function(str, id)
	assert(str and id, "wrong number of arguments to 'SaveSystemNotes2'")
	assert(type(str) == "string", "bad argument #1 to 'SaveSystemNotes2' (string expected, got "..type(str)..")")
	assert(type(id) == "number", "bad argument #2 to 'SaveSystemNotes2' (number expected, got "..type(id)..")")

	local tbl = {timestamp = os.time(), data = str}
	SaveSystemNotes("return unspickle([["..spickle(tbl).."]])", id)
end)
