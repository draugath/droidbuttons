-- I HATE writing GUIs
DroidButtons.ui = {}
local ui = DroidButtons.ui
local vars = DroidButtons.vars
local db = DroidButtons

local _, _, msgdlgtemplate3 = dofile("templates.lua")

local f, err = loadfile("ui_edit.lua")
if (f) then f(announcement_dialog) else console_print(err) end


local bool_map  = {[true] = 'ON', [false] = 'OFF'}
local state_map = {ON = true,  OFF = false}
local val_map   = {[true] = 1, [false] = 0}

local wInt = gkini.WriteInt
local wStr = gkini.WriteString

local function check_number(i, min, max)
	i = tonumber(i); min = tonumber(min); max = tonumber(max)
	if     (not i)           then return false 
	elseif (min and i < min) then return false
	elseif (max and i > max) then return false
	end
	return true
end

local function make_dirty(ctl, num)
	if (num) then 
		local result = check_number(num, 1)
		if (result) then ctl.fgcolor = "255 255 255"; vars.TabState.BadValue = false
		else             ctl.fgcolor = "255 0 0"; vars.TabState.BadValue = true
		end
	end
	ui.Tabs:MakeDirty()
end

local function px(i) return tonumber(HUDSize(i/100, 0):match("(%d+)x")) end
local function py(i) return tonumber(HUDSize(0, i/100):match("x(%d+)")) end

local function create_subdlg(ctrl)
	local dlg = iup.dialog{ctrl, border="NO", menubox="NO", resize="NO", bgcolor="0 0 0 0 *",}
	dlg.visible = "YES"
	return dlg
end

local function create_scrollbox(content)
	local scrollbox = iup.stationsublist{{}, control = "YES", expand = "YES",}
	iup.Append(scrollbox, create_subdlg(content))
	scrollbox[1] = 1
	return scrollbox
end



--[[ *******************************************************************************
     *                                                                             *
     * Miscellaneous Dialogs                                                       *
     *                                                                             *
     ******************************************************************************* ]]
local save_discard_cancel_dialog = msgdlgtemplate3() -- Configured in ui:BuildMainDialog()

local yes_no_dialog = msgdlgtemplate2() -- Configured in ui:BuildButtonsTab()

local announcement_dialog = msgdlgtemplate1()
announcement_dialog:SetMessage(
	"One or more fields contain an invalid value.",
	"OK",
	function(self) HideDialog(iup.GetDialog(self)) end
)


--[[ *******************************************************************************
     *                                                                             *
     * BuildButtonsTab()                                                           *
     *                                                                             *
     ******************************************************************************* ]]
function ui:BuildButtonsTab()
	local buttons_tab
	local buttonlist
	local editcontrols
	local addbutton, copybutton, deletebutton

	buttonlist = iup.stationsublist{size = "%35x", expand = "VERTICAL"}

	editcontrols = self:BuildEditRegionInterface()
	addbutton = iup.stationbutton{title = 'Add', touchhover = "YES"}
	copybutton = iup.stationbutton{title = 'Copy', touchhover = "YES", active = "NO"}
	deletebutton = iup.stationbutton{title = 'Delete', touchhover = "YES", active = 'NO'}

	buttons_tab = iup.pdarootframebg{
		iup.hbox{
			iup.vbox{
				iup.hbox{buttonlist, editcontrols, gap = "%1"},
				iup.hbox{iup.fill{}, addbutton, iup.fill{}, copybutton, iup.fill{}, deletebutton, iup.fill{}, size = "%35", expand = "NO"},
				iup.fill{},
			},
			--iup.fill{},
		},
		border = string.format("%d %d %d %d", px(1), py(2), px(1), py(2)), -- left, top, right, bottom
		tabtitle = 'Buttons',
	}
	-- ----------------------------------------------------------------

	local function reset_buttons()
		copybutton.active = "NO"
		deletebutton.active = "NO"
	end

	function addbutton:action()
		if (vars.TabState.IsDirty) then 
			save_discard_cancel_dialog:SetMessage("The current region has unsaved changes.")
			if (PopupDialog(save_discard_cancel_dialog, iup.CENTER, iup.CENTER) == 3) then return iup.IGNORE end
		end

		buttonlist.value = 0
		buttonlist.cursel = nil
		editcontrols:PopulateFields(#db.buttondefs + 1)
		make_dirty()
	end
	
	function copybutton:action()
		if (vars.TabState.IsDirty) then 
			save_discard_cancel_dialog:SetMessage("The current region has unsaved changes.")
			if (PopupDialog(save_discard_cancel_dialog, iup.CENTER, iup.CENTER) == 3) then return iup.IGNORE end
		end
		
		local buttondef_copy = {}
		for k, v in pairs(db.buttondefs[tonumber(buttonlist.value)]) do
			if (k == "title") then v = "Copy of "..v end
			buttondef_copy[k] = v
		end

		local new_index = #db.buttondefs + 1
		db.buttondefs[new_index] = buttondef_copy
		db:save_buttondefs()
		db:CreateButton(new_index)
		buttons_tab:PopulateTab(new_index)
	end

	function deletebutton:action()
		local index = tonumber(buttonlist.value)
		if (index < 1) then return end

		local retval = PopupDialog(yes_no_dialog, iup.CENTER, iup.CENTER)
		if (retval == 2) then return iup.IGNORE end
		db:DeleteButton(index)
		table.remove(db.buttondefs, index)
		db:save_buttondefs()

		buttons_tab:PopulateTab(0) -- Clear button selection and reset fields/buttons
	end

	
	function buttonlist:action(t, i, v)
		if (v == 1) then 
			if (i == tonumber(self.cursel)) then 
				-- If the user selects the currently selected item in the listbox
				return iup.IGNORE 

			elseif (vars.TabState.IsDirty) then 
				save_discard_cancel_dialog:SetMessage("The current region has unsaved changes.")
				if (PopupDialog(save_discard_cancel_dialog, iup.CENTER, iup.CENTER) == 3) then 
					self.value = self.cursel or 0
					return iup.IGNORE 
				end
			end

			editcontrols:PopulateFields(i)
			self.cursel = i
			if (i == 0) then
				copybutton.active = "NO"
				deletebutton.active = "NO"
			else
				copybutton.active = "YES"
				deletebutton.active = "YES"
			end
		end
	end
				
	yes_no_dialog:SetMessage(
		"Are you sure you want to delete this button?", 
		"Yes",
		function(self) 
			local dlg = iup.GetDialog(self)
			dlg.RetVal = {1}
			HideDialog(dlg)
		end,
		"No",
		function(self)
			local dlg = iup.GetDialog(self)
			dlg.RetVal = {2}
			HideDialog(dlg)
		end
	)

	function buttons_tab:PopulateTab(index)
		-- index = list index that should be selected upon populating the list
		for i, v in ipairs(db.buttondefs) do 
			local name = v.title or '' -- TODO remove default and test for cause of nil title
			if (name == '') then name = v.command1 or v.command2 or v.type or 'No Title' end
			buttonlist[i] = name..' ('..v.type..')'
		end
		buttonlist[#db.buttondefs + 1] = nil

		if (index) then
			buttonlist.value = index
			buttonlist:action(nil, index, 1)
		end
	end

	function buttons_tab:Save()
		return editcontrols:Save()
	end
		
	
	function buttons_tab:Undo()
		return editcontrols:Undo()
	end
	
	-- ----------------------------------------------------------------

	return buttons_tab
end


--[[ *******************************************************************************
     *                                                                             *
     * BuildAccelTab()                                                             *
`     *                                                                             *
     ******************************************************************************* ]]
function ui:BuildAccelTab()
	local accel_tab
	local xaxiscmd, yaxiscmd, zaxiscmd, custom_accel
	
	xaxiscmd = iup.stationsublist(vars.joycmdlist)
	yaxiscmd = iup.stationsublist(vars.joycmdlist)
	zaxiscmd = iup.stationsublist(vars.joycmdlist)
	custom_accel = iup.stationtoggle{title = 'Use Custom Accelerometer Bindings'}
	
	accel_tab = iup.pdarootframebg{
		iup.pdasubframe_nomargin{
			iup.hbox{
				iup.vbox{
					custom_accel,
					iup.hbox{iup.label{title = 'Left/Right:'}, iup.fill{}, iup.pdasubframebg{xaxiscmd}},
					iup.hbox{iup.label{title = 'Fore/Back:'}, iup.fill{}, iup.pdasubframebg{yaxiscmd}},
					--iup.hbox{iup.label{title = 'Z-axis:'}, iup.fill{}, iup.pdasubframebg{zaxiscmd}},
					iup.fill{},
					gap = "%3",
					expand = "VERTICAL",
				},
				iup.fill{},
			},
		},
		border = string.format("%d %d %d %d", px(0.5), py(1), px(0.5), py(1)), -- left, top, right, bottom
		tabtitle = 'Accelerometer',
	}
	accel_tab[1].border = string.format("%d %d %d %d", px(1), py(2), px(1), py(2)) -- left, top, right, bottom
	-- ----------------------------------------------------------------

	function custom_accel:action() make_dirty(self) end
	function xaxiscmd:action(t, i, v)
		if (i == tonumber(self.cursel)) then return iup.IGNORE end
		if (v == 1) then self.cursel = i; make_dirty() end
	end
	function yaxiscmd:action(t, i, v)
		if (i == tonumber(self.cursel)) then return iup.IGNORE end
		if (v == 1) then self.cursel = i; make_dirty() end
	end
	function zaxiscmd:action(t, i, v)
		if (i == tonumber(self.cursel)) then return iup.IGNORE end
		if (v == 1) then self.cursel = i; make_dirty() end
	end
	
	function accel_tab:PopulateTab()
		custom_accel.value = bool_map[db.use_custom_accel]
		xaxiscmd.value = vars.joycmdlist2[(db.accelx)]
		xaxiscmd.cursel = xaxiscmd.value
		yaxiscmd.value = vars.joycmdlist2[(db.accely)]
		yaxiscmd.cursel = yaxiscmd.value
		--zaxiscmd.value = vars.joycmdlist2[(db.accelz)]
		--zaxiscmd.cursel = zaxiscmd.value
	end

	function accel_tab:Save()
		local ov = db.use_custom_accel
		db.use_custom_accel = state_map[custom_accel.value]
		db.accelx = vars.joycmdlist3[tonumber(xaxiscmd.value)]
		db.accely = vars.joycmdlist3[tonumber(yaxiscmd.value)]
		--db.accelz = vars.joycmdlist3[tonumber(zaxiscmd.value)]

		wInt(db.major, 'CustomAccel', val_map[db.use_custom_accel])
		wStr(db.major, 'AccelX', db.accelx)
		wStr(db.major, 'AccelY', db.accely)
		--wStr(db.major, 'AccelZ', db.accelz)

		if (not db.use_custom_accel and db.use_custom_accel ~= ov) then
			announcement_dialog:SetMessage("To properly reset the Accelerometer:\n\n1) Open Options > Advanced > Controls > Accelerometer\n2) Reselect your preferred control option\n3) Save your changes")
			PopupDialog(announcement_dialog, iup.CENTER, iup.CENTER)
		end
		db:set_accel_commands()

		return true
	end
	
	function accel_tab:Undo()
		return self:PopulateTab()
	end
	-- ----------------------------------------------------------------
	return accel_tab
end


--[[ *******************************************************************************
     *                                                                             *
     * BuildSupportTab()                                                           *
     *                                                                             *
     ******************************************************************************* ]]
function ui:BuildSupportTab()
	local support_tab
	local vo_forum_button
	local vo_ticket_button
	local voupr_button
	local voupr_db_button
	local db_thread_button
	local db_ticket_button
	
	local support_button_size

	voupr_button = iup.stationbutton{title = "Vendetta Online Unofficial Plugin Repository"}
		-- Get the size of the largest button for uniformity
		iup.Map(voupr_button)
		support_button_size = voupr_button.size
	vo_forum_button = iup.stationbutton{title = "Vendetta Online Forums", size = support_button_size}
	vo_ticket_button = iup.stationbutton{title = "Vendetta Online Support Ticket", size = support_button_size}
	voupr_db_button = iup.stationbutton{title = "VOUPR: DroidButtons", size = support_button_size}
	db_thread_button = iup.stationbutton{title = "DroidButtons Discussion Thread", size = support_button_size}
	db_ticket_button = iup.stationbutton{title = "DroidButtons Issue Tracker", size = support_button_size}

	support_tab = iup.pdarootframebg{
		iup.pdasubframe_nomargin{
			iup.hbox{
				iup.vbox{
					vo_forum_button,
					vo_ticket_button,
					voupr_button,
					voupr_db_button,
					db_thread_button,
					db_ticket_button,
					iup.fill{},
					gap = "%4",
				},
				iup.fill{},
			},
		},
		border = string.format("%d %d %d %d", px(0.5), py(1), px(0.5), py(1)), -- left, top, right, bottom
		tabtitle = 'Support',
	}
	support_tab[1].border = string.format("%d %d %d %d", px(1), py(2), px(1), py(2)) -- left, top, right, bottom

	-- ----------------------------------------------------------------

	function vo_forum_button:action() Game.OpenWebBrowser("http://www.vendetta-online.com/x/msgboard/") end
	function vo_ticket_button:action() Game.OpenWebBrowser("http://www.vendetta-online.com/x/support") end
	function voupr_button:action() Game.OpenWebBrowser("http://www.voupr.com") end
	function voupr_db_button:action() Game.OpenWebBrowser("http://www.voupr.com/plugin.php?name=droidbuttons") end
	function db_thread_button:action() Game.OpenWebBrowser("http://www.vendetta-online.com/x/msgboard/16/27594") end
	function db_ticket_button:action() Game.OpenWebBrowser("https://bitbucket.org/draugath/droidbuttons/issues?status=new&status=open") end

	return support_tab
end


--[[ *******************************************************************************
     *                                                                             *
     * BuildOptionsTab()                                                           *
     *                                                                             *
     ******************************************************************************* ]]
function ui:BuildOptionsTab()
	local options_tab, basic_sub_tab, adv_sub_tab
	local button_width, button_height
	local field_width, field_height
	local font, delay
	local snaptogrid, gridsize
	local lockimage, lockbutton, remove_default_ui, im_size, bt_size
	local touchtotarget_onoff, touchtotarget_cmd

	local unlock_timer = Timer()
	local remove_default_ui_confirmation_dialog, use_default_ui_confirmation_dialog

	local function ms(v) return iup.label{title = "ms", visible = v and "YES" or "NO"} end
	
	
	button_width = iup.text{size = px(7.5)} 
	button_height = iup.text{size = px(7.5)}

	field_width = iup.text{size = px(7.5)} 
	field_height = iup.text{size = px(7.5)}
	
	font = iup.text{size = px(7.5)}
	delay = iup.text{size = px(7.5)}
	
	snaptogrid = iup.stationtoggle{title = 'Snap-to-Grid'}
	gridsize = iup.text{size = px(7.5)}
	
	im_size = Font.Default * 2 > 24 and Font.Default * 2 or 24
	bt_size = Font.Default * 2 > 24 and Font.Default * 2 + 8 or 32
	lockimage = iup.label{title = '', image = db.IMAGE_DIR..'Lock.png', size = im_size..'x'..im_size, bgcolor = '255 255 255 255 *'}
	lockbutton = iup.stationbutton{title = '', size = bt_size..'x'..bt_size, bgcolor = '255 255 255 75 +'}
	remove_default_ui = iup.stationtoggle{title = 'Remove Default Touch Interface', active = 'NO'}
	touchtotarget_onoff = iup.stationtoggle{title = 'Enable Touch-to-Target', active = 'NO'}
	touchtotarget_cmd = iup.list(vars.radarcmdlist); touchtotarget_cmd.active = 'NO'
	

	basic_sub_tab = iup.stationsubsubframe{
		iup.hbox{
			iup.vbox{
				iup.vbox{
					iup.label{title = 'Button'},
					iup.vbox{
						iup.hbox{iup.label{title = 'Default Width: '}, iup.fill{}, button_width, ms()},
						iup.hbox{iup.label{title = 'Default Height: '}, iup.fill{}, button_height, ms()},
						gap = "%1",
					},
				},
				iup.vbox{
					iup.label{title = 'Field'},
					iup.vbox{
						iup.hbox{iup.label{title = 'Default Width: '}, iup.fill{}, field_width, ms()},
						iup.hbox{iup.label{title = 'Default Height: '}, iup.fill{}, field_height, ms()},
						gap = "%1",
					},
				},
				iup.hbox{iup.label{title = 'Font Size: '}, iup.fill{}, font, ms()},
				iup.hbox{iup.label{title = 'Long-press delay: '}, iup.fill{}, delay, ms(true)},
				iup.vbox{
					snaptogrid,
					iup.hbox{iup.label{title = 'Grid Size: '}, iup.fill{}, gridsize, ms()},
					gap = "%1",
				},
				iup.fill{},
				gap = "%3",
				expand = "VERTICAL",
			},
			iup.fill{},
		},
		border = string.format("%d %d %d %d", px(1), py(2), px(1), py(2)), -- left, top, right, bottom
		tabtitle = 'Basic',
	}

	adv_sub_tab = iup.stationsubsubframe{
		iup.vbox{
			iup.hbox{
				iup.zbox{
					iup.vbox{
						iup.fill{size = 4},
						iup.hbox{iup.fill{size = 4}, lockimage, iup.fill{size = 4}},
						iup.fill{size = 4},
					},
					lockbutton,
					all = 'YES',
				},
				iup.vbox{
					remove_default_ui, 
					iup.vbox{
						touchtotarget_onoff,
						iup.pdasubframebg{touchtotarget_cmd},
						gap = "%1",
					},
					gap = "%3",
				},
				iup.fill{},
				gap = "%3",
			},
			iup.fill{},
		},
		border = string.format("%d %d %d %d", px(1), py(2), px(1), py(2)), -- left, top, right, bottom
		tabtitle = 'Advanced',
	}

	options_tab = iup.pda_sub_tabs{
		basic_sub_tab,
		adv_sub_tab,

	}

	iup.Detach(options_tab[1][1][1][2]) -- remove the vbox fill that is screwing things up
	iup.Detach(options_tab[1][1][1][1][2]) -- remove the hbox fill that is screwing things up
	options_tab[1][1][1].margin = string.format("%dx%d", px(0.5), py(1))
	options_tab[1][1][1][1][1].alignment = "NW"
	options_tab.tabtitle = "Options"
	-- ----------------------------------------------------------------

	local function lock_advanced_controls()
		unlock_timer:Kill()
		remove_default_ui.active = 'NO'
		touchtotarget_onoff.active = 'NO'
		touchtotarget_cmd.active = 'NO'
		touchtotarget_cmd.fgcolor = "128 128 128"
		iup.GetParent(touchtotarget_cmd).bgcolor = "128 128 128 255 *"
		lockbutton.active = "YES"
		lockimage.image = db.IMAGE_DIR..'Lock.png'
	end
		
	local function unlock_advanced_controls()
		remove_default_ui.active = 'YES'
		touchtotarget_onoff.active = 'YES'
		touchtotarget_cmd.active = 'YES'
		touchtotarget_cmd.fgcolor = "255 255 255"
		iup.GetParent(touchtotarget_cmd).bgcolor = "255 255 255 255 *"
		lockbutton.active = "NO"
		lockimage.image = db.IMAGE_DIR..'Open_lock_disabled.png'
	end

	local function reset_fields()
		-- Basic Tab
		button_width.fgcolor = "255 255 255"
		button_height.fgcolor = "255 255 255"
		field_width.fgcolor = "255 255 255"
		field_height.fgcolor = "255 255 255"
		font.fgcolor = "255 255 255"
		delay.fgcolor = "255 255 255"
		gridsize.fgcolor = "255 255 255"

		-- Advanced Tab
		lock_advanced_controls() -- TODO: redundant?

		vars.TabState.BadValue = false
	end
	

	do	
		local function func(self, c, after)
			if (c == 27 and after == "") then return end
			make_dirty(self, after)
		end
		button_width.action = func
		button_height.action = func
		field_width.action = func
		field_height.action = func
		font.action = func
		delay.action = func
		gridsize.action = func
	end

	
	function snaptogrid:action(v) make_dirty() end

	function touchtotarget_onoff:action() make_dirty(self) end
	function touchtotarget_cmd:action(t, i, v)
		if (i == tonumber(self.cursel)) then return iup.IGNORE end
		if (v == 1) then self.cursel = i; make_dirty() end
	end

	
	function lockbutton:action()
		if (lockimage.image == db.IMAGE_DIR..'Lock.png') then
			lockimage.image = db.IMAGE_DIR..'Open_lock.png'
			remove_default_ui.active = 'YES'
			unlock_timer:SetTimeout(5000, lock_advanced_controls)
		else
			lock_advanced_controls()
		end
	end

	remove_default_ui_confirmation_dialog = msgdlgtemplate2(
		"By selecting this, you are choosing to remove the default UI and create your own.\n\n\127ff0000Do not contact Guild Software regarding any problems experienced with this \nfeature enabled or this plugin installed.\n\n\127ffffffDo you wish to proceed?",
		'OK',
		function(self) unlock_advanced_controls(); iup.GetDialog(self).RetVal = {1}; HideDialog(iup.GetDialog(self)) end,
		'Cancel',
		function(self) remove_default_ui.value = "OFF"; lock_advanced_controls(); iup.GetDialog(self).RetVal = {2}; HideDialog(iup.GetDialog(self)) end
	)
	function remove_default_ui_confirmation_dialog:show_cb() unlock_timer:Kill() end

	use_default_ui_confirmation_dialog = msgdlgtemplate2(
		"In order to re-enable to default touch interface, it will be necessary to\nreload the interface.\n\nIf you click OK, this will be done automatically for you upon clicking 'Save'.\n\n\127ffffffDo you wish to proceed?",
		'OK',
		function(self) lock_advanced_controls(); iup.GetDialog(self).RetVal = {1}; HideDialog(iup.GetDialog(self)) end,
		'Cancel',
		function(self) remove_default_ui.value = 'ON'; iup.GetDialog(self).RetVal = {2}; HideDialog(iup.GetDialog(self)) end
	)

	function remove_default_ui:action(v)
		if (v == 1) then
			if (PopupDialog(remove_default_ui_confirmation_dialog, iup.CENTER, iup.CENTER) == 2) then
				return iup.IGNORE
			end
		else
			if (state_map[self.value] ~= db.disable_default_ui) then
				if (PopupDialog(use_default_ui_confirmation_dialog, iup.CENTER, iup.CENTER) == 2) then 
					return iup.IGNORE
				end
			else
				lock_advanced_controls()
				return iup.DEFAULT
			end
		end
		make_dirty()
		vars.DialogResult = nil
	end

	function options_tab:PopulateTab()
		-- Basic Tab
		button_width.value = db.defaults.button_width
		button_height.value = db.defaults.button_height
		field_width.value = db.defaults.field_width
		field_height.value = db.defaults.field_height
		font.value = db.defaults.font
		delay.value = db.long_press_delay
		snaptogrid.value = bool_map[db.snap_to_grid]
		gridsize.value = db.grid_size
		
		--Advanced Tabs
		remove_default_ui.value = bool_map[db.disable_default_ui]
		touchtotarget_onoff.value = bool_map[db.enable_touch_to_target]
		touchtotarget_cmd.value = vars.radarcmdlist2[db.touch_to_target_command]
		if (Subplatform == "XperiaPlay") then
			lockbutton.active = 'NO'
			lockimage.image = db.IMAGE_DIR..'Lock_disabled.png'
		elseif (db.disable_default_ui) then 
			unlock_advanced_controls()
		else
			lock_advanced_controls()
		end
	end
	
	function options_tab:Save()
		if (vars.TabState.BadValue) then return false end
		local nv, act
		
		-- Basic Tab
		db.defaults.button_width = tonumber(button_width.value)
		db.defaults.button_height = tonumber(button_height.value)
		db.defaults.field_width = tonumber(field_width.value)
		db.defaults.field_height = tonumber(field_height.value)
		db.defaults.font = tonumber(font.value)
		db.long_press_delay = tonumber(delay.value)
		db.snap_to_grid = state_map[snaptogrid.value]
		db.grid_size = tonumber(gridsize.value)
		
		wInt(db.major, 'SnapToGrid', val_map[db.snap_to_grid])
		wInt(db.major, 'GridSize', db.grid_size)
		wInt(db.major, 'LongPressDelay', db.long_press_delay)
		wInt(db.major, 'DefaultWidth', db.defaults.button_width)
		wInt(db.major, 'DefaultHeight', db.defaults.button_height)
		wInt(db.major, 'DefaultFieldWidth', db.defaults.field_width)
		wInt(db.major, 'DefaultFieldHeight', db.defaults.field_height)
		wInt(db.major, 'DefaultFont', db.defaults.font)

		-- Advanced Tab

		nv = state_map[touchtotarget_onoff.value]
		if (db.enable_touch_to_target ~= nv) then act = nv and 3 or 4 end
		db.enable_touch_to_target = nv

		nv = state_map[remove_default_ui.value]
		if (db.disable_default_ui ~= nv) then act = nv and 1 or 2 end
		db.disable_default_ui = nv

		db.touch_to_target_command = vars.radarcmdlist[tonumber(touchtotarget_cmd.value)]
		
		wInt(db.major, 'DisableDefaultUI', val_map[db.disable_default_ui])
		wInt(db.major, 'TouchToTarget', val_map[db.enable_touch_to_target])
		wStr(db.major, 'TouchToTargetCmd', db.touch_to_target_command)

		local actions = {
			function() db.remove_default_ui(); HUD:CreateTouchLayer() end, -- remove default
			db.reload, -- restore default
			function() db:CreateTouchToTargetRegion() end, -- enable ttt
			function() db:DestroyTouchToTargetRegion() end, -- disable ttt
		}

		return actions[act] or true
	end
	
	function options_tab:Undo()
		reset_fields()
		return self:PopulateTab()
	end
	-- ----------------------------------------------------------------

	return options_tab
end


--[[ *******************************************************************************
     *                                                                             *
     * BuildMainDialog()                                                           *
     *                                                                             *
     ******************************************************************************* ]]
function ui:BuildMainDialog()
	local dlg, content
	local undobutton, savebutton, closebutton

	self.ButtonsTab = self:BuildButtonsTab()
	self.AccelTab = self:BuildAccelTab()
	self.OptionsTab = self:BuildOptionsTab()
	self.SupportTab = self:BuildSupportTab()

	self.Tabs = iup.pda_root_tabs{
		self.ButtonsTab,
		self.AccelTab,
		self.OptionsTab,
		self.SupportTab,
	}
--	iup.Detach(tabs[1][4][1][2]) -- remove the fill from tab bar that is screwing things up
-- 	iup.Detach(tabs[2][1][2]) -- remove the vbox fill that is screwing things up
	iup.Detach(self.Tabs[2][1][1][2]) -- remove the hbox fill that is screwing things up

	undobutton = iup.stationbutton{title = "Undo", touchhover = "YES", active = "NO"}
	savebutton = iup.stationbutton{title = "Save", touchhover = "YES", active = "NO"}
	closebutton = iup.stationbutton{title = "Close", touchhover = "YES"}

	
	content = iup.pdarootframe{
			iup.vbox{
				self.Tabs,
				iup.hbox{iup.label{title = "Version "..db.version}, iup.fill{}, undobutton, savebutton, iup.fill{size = "%5"}, closebutton, gap = 15, },
				gap = 7,
				margin = px(0.5).."x"..py(1),
			},
	}

	dlg = iup.dialog{
		content,
		defaultesc = closebutton,
		bgcolor = "0 0 0 *",
		fullscreen="YES",
		title = 'DroidButtons v'..DroidButtons.version..': Button Management',
		border = "NO",
		resize = "NO",
		menubox = "YES",
		topmost = "YES",
	}
	iup.Map(dlg)

	-- ----------------------------------------------------------------

	local function reset_buttons()
		ui.Tabs:SetTabTextColor(tonumber(ui.Tabs:GetTab().Index), ui.Tabs.seltextcolor)
		vars.TabState.IsDirty = false
		savebutton.active = "NO"
		undobutton.active = "NO"
	end

	function closebutton:action()
		if (vars.TabState.IsDirty) then 
			save_discard_cancel_dialog:SetMessage("The current tab has unsaved changes.")
			if (PopupDialog(save_discard_cancel_dialog, iup.CENTER, iup.CENTER) == 3) then return iup.IGNORE end
		end
		HideDialog(iup.GetDialog(self))
	end

	function savebutton:action()
		local result = ui.Tabs:GetTab():Save()
		if (result) then 
			reset_buttons() 
			if (type(result) == "function") then result() end
		else
			announcement_dialog:SetMessage("One or more fields contain an invalid value.")
			PopupDialog(announcement_dialog, iup.CENTER, iup.CENTER)
		end
		return result
	end

	function undobutton:action()
		local result = ui.Tabs:GetTab():Undo()
		reset_buttons()
		if (type(result) == "function") then result() end
	end
	
	save_discard_cancel_dialog:SetMessage(
		"The current tab has unsaved changes.", 
		"Save",
		function(self) 
			if (not savebutton:action()) then return iup.IGNORE end -- if the Apply fails.
			local dlg = iup.GetDialog(self)
			dlg.RetVal = {1}
			HideDialog(dlg)
		end,
		"Discard",
		function(self)
			undobutton:action()
			local dlg = iup.GetDialog(self)
			dlg.RetVal = {2}
			HideDialog(dlg)
		end,
		"Cancel",
		function(self)
			local dlg = iup.GetDialog(self)
			dlg.RetVal = {3}
			HideDialog(dlg)
		end
	)
		
	function self.Tabs:MakeDirty()
		self:SetTabTextColor(tonumber(self:GetTab().Index), "255 0 0")
		vars.TabState.IsDirty = true
		savebutton.active = "YES"
		undobutton.active = "YES"
	end

	function dlg:Show()
		ui.ButtonsTab:PopulateTab()
		ui.AccelTab:PopulateTab()
		ui.OptionsTab:PopulateTab()
		ShowDialog(self)
	end
	
	-- ----------------------------------------------------------------

	local i, curbutton, curtab = 1, self.Tabs:GetTabButton(1)
	
	while (curbutton) do
		if (iup.GetType(curbutton) == "button") then
			self.Tabs:SetTab(i)
			curtab = self.Tabs:GetTab()
			curtab.Index = i
			curbutton.Index = i
			curbutton.oldAction = curbutton.action
			function curbutton:action()
				if (tonumber(self.Index) ~= tonumber(ui.Tabs:GetTab().Index)) then
					if (vars.TabState.IsDirty) then 
						save_discard_cancel_dialog:SetMessage("The current tab has unsaved changes.")
						local retval = PopupDialog(save_discard_cancel_dialog, iup.CENTER, iup.CENTER)
						if (retval == 3) then return iup.IGNORE end
					end
					return self.oldAction(self)
				end
			end
		end	
		i = i + 1
		curbutton = self.Tabs:GetTabButton(i)
	end
	self.Tabs:SetTab(2)
	-- ----------------------------------------------------------------

	return dlg
end

ui.MainDialog = ui:BuildMainDialog()
