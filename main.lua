declare('DroidButtons', {
	data_version = 2,
	hudsize = HUDSize(1,1),
	IMAGE_DIR = 'plugins/droidbuttons/images/',
	major = 'DroidButtons',
	notesid = 37643,
	version = '2.3.1',
	xres = gkinterface.GetXResolution(),
	yres = gkinterface.GetYResolution(),
})

if (not gkinterface.IsTouchModeEnabled()) then
	console_print("DroidButtons not loaded. You are not playing on a device with a touch screen.")
	return
else
	console_print('DroidButtons loaded (v'..DroidButtons.version..'). HUD size is '..HUDSize(1,1))
end

-- localized functions
local tinsert = table.insert
local rInt = gkini.ReadInt
local wInt = gkini.WriteInt
local rStr = gkini.ReadString
local wStr = gkini.WriteString

local function ispluscommand(str) 
	if (type(str) == 'string') then
		if (str:match('^+')) then return true end
	end
	return false
end

local function xor(a, b) return ((a and not b) or (not a and b)) end
local function px(i) return tonumber(HUDSize(i/100, 0):match("(%d+)x")) end
local function py(i) return tonumber(HUDSize(0, i/100):match("x(%d+)")) end

-- local variables
local hud_type -- this will be set by the HUD_SHOW event and cleared by the HUD_HIDE event
local mouselook_cmd = {
	Turn = "+LookX",
	Pitch = "+LookY",
}

-- initialize environment

local db = DroidButtons

db.first_run = rInt(db.major, "FirstRun", 0) == 0
db.snap_to_grid = rInt(db.major, 'SnapToGrid', 1) == 1
db.grid_size = rInt(db.major, 'GridSize', 16)
db.long_press_delay = rInt(db.major, 'LongPressDelay', 1000)
db.disable_default_ui = rInt(db.major, 'DisableDefaultUI', 0) == 1
db.use_custom_accel = rInt(db.major, 'CustomAccel', 0) == 1
db.accelx = rStr(db.major, 'AccelX', 'NONE')
db.accely = rStr(db.major, 'AccelY', 'NONE')
db.accelz = rStr(db.major, 'AccelZ', 'NONE')
db.enable_touch_to_target = rInt(db.major, 'TouchToTarget', 1) == 1
db.touch_to_target_command = rStr(db.major, 'TouchToTargetCmd', 'RadarNextFront')
if (db.disable_default_ui and Subplatform == "XperiaPlay") then db.disable_default_ui = false end

do
	local x1, x2 = px(5.65), px(16)
	db.defaults = {
		button_width = rInt(db.major, 'DefaultWidth', x1 > 64 and x1 or 64),
		button_height = rInt(db.major, 'DefaultHeight', x1 > 64 and x1 or 64), -- want the same height as width
		field_width = rInt(db.major, 'DefaultFieldWidth', x2 > 200 and x2 or 200),
		field_height = rInt(db.major, 'DefaultFieldHeight', x2 > 200 and x2 or 200), -- want the same height as width
		font = rInt(db.major, 'DefaultFont', Font.Default),
		fgcolor = '',
		bgcolor_button = '255 255 255 100 *',
		bgcolor_field = '255 255 255 255 *',
	}
end

db.buttondefs = {--[[
	title = <string>,
	type = <'button'|'field'>,
	command1 = <string>,
	command2 = <string>, -- optional
	command2cancel = <string>, -- optional
	font = <number>,
	x = <number>,
	y = <number>,
	w = <number>,
	h = <number>,
	invertX = <boolean>, -- optional; for fields only
	invertY = <boolean>, -- optional; for fields only
	fgcolor = <string>, -- optional; untested
	bgcolor = <string>, -- optional; untested
]]}

db.buttons = {} -- Normally this wouldn't need to be instantiated so soon, but a fresh install of the game on Android opens the HUD before login.
db.regions = {} -- will hold index keys matching Touch Regions with values pointing to the button index they represent

declare('dar', function(start, count)
	start = start or 0
	count = count or 10000
	
	for i = start, start + count do
		gkinterface.DestroyTouchRegion(i)
	end
	--ReloadInterface()
end)

declare('pr', function() 
	radar.val = radar.val or 0
	console_print("\nregions "..string.rep(radar.val, 64))
	radar.val = radar.val + 1
	local hudregionlist = {}
	for key, val in pairs(HUD) do
		if (key:match("region")) then table.insert(hudregionlist, key) end
	end
	table.sort(hudregionlist)
	for _, key in ipairs(hudregionlist) do console_print(key..": "..HUD[key]) end
	printtable(db.regions)
end)

function db.reload()
	print('\127ffffffReloading...')
	ReloadInterface()
end

-- hooked functions
local old_SetMouseLookMode = gkinterface.SetMouseLookMode
function gkinterface.SetMouseLookMode(bool)
	old_SetMouseLookMode(bool)
	if (GetPlayerName() and hud_type) then -- are we in game and do we have a HUD?
		db:DestroyRegions('field')
		db:CreateRegions('field')
	end
end

-- supporting functions
function db.remove_default_ui()
	HUD:DestroyAimTouchRegion()
	-- Detach it instead of visible = 'NO', since for now it appears to be made visible again when HUD.dlg is shown.
	-- Don't destroy it, since there are other pieces of code that in the default IF that may need to reference it
	iup.Detach(HUD.touchdisplaylayer)

	-- Don't worry about saving these functions. Until ray separates the two parts of CreateTouchLayer,
	-- a ReloadInterface() is going to be necessary to fully restore functionality anyway.
	HUD.CreateAimTouchRegion = function() end
	HUD.CreateTouchLayer = function(self) 
		local chat_w, chat_h = HUD.chatcontainer.chattext.size:match("(%d+)x(%d+)")
		local def = {x = 0, y = 0, w = chat_w, h = chat_h}
		self:DestroyTouchLayer()
		self.chattouchregion = gkinterface.CreateTouchRegion(nil, nil, "say_channel", false, false, false, false, false, false, def.x, def.y, (def.x + def.w - 1), (def.y + def.h - 1))
		if (db.enable_touch_to_target) then db:CreateTouchToTargetRegion() end
	end
	HUD:CreateTouchLayer()
end

function db:set_accel_commands(init)
	if (self.use_custom_accel) then
		local axis_cmd
		axis_cmd = self.accelx; if (axis_cmd == 'NONE') then axis_cmd = '' end
		gkinterface.BindCommand(2147483648 + 1, axis_cmd)
		axis_cmd = self.accely; if (axis_cmd == 'NONE') then axis_cmd = '' end
		gkinterface.BindCommand(2147483648 + 2, axis_cmd)
		--axis_cmd = self.accelz; if (axis_cmd == 'NONE') then axis_cmd = '' end
		--gkinterface.BindCommand(2147483648 + 3, axis_cmd)
	end
end


local data_changes = {} -- Index is the data version.  Value is a function that will update the data structure from the previous version
data_changes[1] = function(data)
		for _, v in ipairs(data) do
			v.type = 'button'
		end
	end
data_changes[2] = function(data)
		for _, v in ipairs(data) do
			v.visible = true
			if (v.type == 'field') then
				v.nonlinear = nil
				v.Scurve = 256 -- default to unfiltered Linear
				v.Svalue = 100
			end
		end
	end

function db:save_buttondefs()
	for i, bd in ipairs(self.buttondefs) do
		if (bd.title == nil) then
			local save_error_dialog = msgdlgtemplate1()
			save_error_dialog:SetMessage(
				"A problem was detected in the button information before saving. Double-check all button titles.",
				"OK",
				function(self) HideDialog(iup.GetDialog(self)) end
			)
			PopupDialog(save_error_dialog, iup.CENTER, iup.CENTER)
			return
		end
	end
	SaveSystemNotes2(spickle({version = self.data_version, buttondefs = self.buttondefs}), self.notesid)
end

function db:load_buttondefs()
	local save
	local saved_data, isSN2 = LoadSystemNotes2(self.notesid)
	saved_data = unspickle(saved_data)
	if (not isSN2) then
		saved_data = unspickle(LoadSystemNotes(self.notesid))
		if (not saved_data.version) then
			saved_data = {version = 0, buttondefs = saved_data}
		end
	end

	--TODO give the user a chance to abort the update and not load the plugin
	
	-- Update the data structure so the code doesn't have to be riddled with exceptions.
	if (saved_data.version < self.data_version or not isSN2) then
		-- Create a backup of the saved data, just in case.
		local backupid = (self.notesid + saved_data.version) * -1
		local backup_data
		if (saved_data.version == 0) then backup_data = spickle(saved_data.buttondefs) else backup_data = spickle(saved_data) end
		SaveSystemNotes(backup_data, backupid)
		local msg = 'The latest version of DroidButtons has modifed the structure of it\'s saved data.\n'
		msg = msg..'DroidButtons will attempt to apply the appropriate updates without modifying your existing data.\n\n'
		msg = msg..'A backup has been created at "<Vendetta Directory>/settings/'..GetPlayerName()..'/system'..backupid..'notes.txt.'
		OpenAlarm('DroidButtons: Update Notice', msg, 'OK')

		-- apply each incremental
		for i = saved_data.version + 1, self.data_version do
			data_changes[i](saved_data.buttondefs)
		end
		save = true
		
	elseif (saved_data.version > self.data_version) then
		local msg = 'The version of DroidButtons you are using may not be compatible with your saved button data.\n'
		msg = msg .. 'In order to avoid errors or damaging your saved button data, you should install a newer \nversion of DroidButtons.'
		OpenAlarm('DroidButtons: Data Version Mismatch', msg, 'OK')
		
	end
	self.buttondefs = saved_data.buttondefs
	if (save) then self:save_buttondefs() end
end


-- Visual indicators and touch regions will be created/updated/removed in tandem.
-- If a bound command is changed, then the button will need to be removed and re-added.
-- Otherwise, update will resize/move the region along with the button
function db:CreateButton(index, init) -- originally received buttondef, not sure if I need it to still
	-- TODO discover why create is being called multiple times during Add and other actions
	local default = self.defaults
	local buttondef = self.buttondefs[index]

	local SaP = { -- size and position
		x = buttondef.x or 0,
		y = buttondef.y or 0,
		w = buttondef.w or default[buttondef.type.."_width"],
		h = buttondef.h or default[buttondef.type.."_height"],
		font = buttondef.font or default["font"],
	}
	
	-- create the button definition
	local button = {
		title = buttondef.title or '', 
		size = string.format("%dx%d", SaP.w, SaP.h),
		cx = SaP.x,
		cy = SaP.y,
		active = 'YES',
		fgcolor = buttondef.fgcolor,
		bgcolor = buttondef.bgcolor or default['bgcolor_'..buttondef.type],
		centeruv = ".5 .5 .5 .5",
		font = SaP.font,
		visible = (buttondef.visible == nil and "YES") or (buttondef.visible and "YES" or "NO")
	}

	-- create the button
	local b, r, cmd
	if (buttondef.type == 'button') then
		if (buttondef.button_type == "standard" or buttondef.button_type == nil) then
			-- Only directly bind +Commands for handling.  All others are handled in the TOUCH_RELEASED event, regardless of the presence of a command2
			if (ispluscommand(buttondef.command1)) then cmd = buttondef.command1 end

		elseif (buttondef.button_type == "configurable" and #buttondef.commands > 0) then
			button.title = buttondef.commands[buttondef.commands.current].title

		end

		b = iup.stationbutton(button)
		if ((buttondef.command1 and not cmd) or buttondef.command2 or buttondef.commands) then b.Timer = Timer() end	
			
		r = gkinterface.CreateTouchRegion(nil, nil, cmd, false, false, false, false, false, false,
			SaP.x, SaP.y, (SaP.x + SaP.w - 1), (SaP.y + SaP.h - 1), -1)

	elseif (buttondef.type == 'field') then
		button.image = IMAGE_DIR..'hud_radar.png'
		b = iup.label(button)
		local inturret = (hud_type == "turret")
		local cmd1, cmd2 = buttondef.command1, buttondef.command2
		if (inturret or gkinterface.GetMouseLookMode()) then
		    cmd1 = mouselook_cmd[buttondef.command1] or buttondef.command1
		    cmd2 = mouselook_cmd[buttondef.command2] or buttondef.command2
		end
		r = gkinterface.CreateTouchRegion(
			cmd1,     -- 1
			cmd2,     -- 2
			inturret and not buttondef.lookspring and "+LookPress" or buttondef.command3, -- 3
			xor(buttondef.invertX, not (cmd1 and cmd1:match("+Look"))),              -- 4  +LookX has inverted axis from Turn
			xor(buttondef.invertY, not (cmd2 and cmd2:match("+Look"))),              -- 5  +LookY has inverted axis from Pitch
			(inturret and not gkinterface.GetMouseLookMode()) or buttondef.mouselookX,                -- 6
			(inturret and not gkinterface.GetMouseLookMode()) or buttondef.mouselookY,                -- 7
			buttondef.relative,                              -- 8
			not inturret and buttondef.Scurve or false,      -- 9   filtered 0-4 | unfiltered 256-260
			SaP.x,                                           -- 10
			SaP.y,                                           -- 11
			(SaP.x + SaP.w - 1),                             -- 12
			(SaP.y + SaP.h - 1),                             -- 13
			-1, -- Z-order                                   -- 14 lower numbers higher in the stack
			(buttondef.Svalue or 100) / 100,                 -- 15 sensitivity (0.01 - 2) TODO find a better fix to this.  region shouldn't be created before new region is saved
			buttondef.trackingcircles                        -- 16 touch tracking circles On (true | nil) or Off (false)
		)
	end

	b.Bound = cmd
	b.Def = buttondef
	b.Region = r
	self.regions[r] = b
	self.buttons[index] = b
	iup.Append(iup.GetNextChild(self.cbox), b)
	if (not init) then
		self.cbox:map()
		self.cbox.REFRESH = "YES"
	end
end

function db:UpdateButton(index, newdef, ub, ur)
	newdef = newdef or {}
	ub = ub == nil and true or ub -- update button
	ur = ur == nil and true or ur -- update region
	local buttondef = self.buttondefs[index]
	local button = self.buttons[index]

	if (button) then
		local isvisible = newdef.visible or buttondef.visible
		newdef = {
			title = newdef.title or buttondef.title or "",
			type = newdef.type or buttondef.type,
			x = newdef.x or buttondef.x,
			y = newdef.y or buttondef.y,
			w = newdef.w or buttondef.w,
			h = newdef.h or buttondef.h,
			font = newdef.font or buttondef.font,
			visible = (isvisible == nil and "YES") or (isvisible and "YES" or "NO")
		}
		if (buttondef.commands and #buttondef.commands > 0) then newdef.title = buttondef.commands[buttondef.commands.current].title end
		
		if (ub) then
			button.title = newdef.title
			button.cx = newdef.x
			button.cy = newdef.y
			button.size = string.format("%dx%d", newdef.w, newdef.h)
			button.font = newdef.font
			button.visible = newdef.visible

			if (buttondef.type == 'button') then
				if ((buttondef.command1 and not ispluscommand(buttondef.command1)) or buttondef.command2) then 
					button.Timer = Timer() 
				else
					button.Timer = nil
				end
			end
			self.cbox.REFRESH = "YES"
		end
		
		if (ur) then
			gkinterface.ChangeTouchRegionSize(tonumber(button.Region), newdef.x, newdef.y, (newdef.x + newdef.w - 1), (newdef.y + newdef.h - 1))
		end
	end
end

function db:DeleteButton(index, teardown)
	-- if teardown is true, don't remove the buttons from their table  -- TODO why am I doing this?  This doesn't seem necessary
	local b = db.buttons[index]
	local r = tonumber(b.Region)
	gkinterface.DestroyTouchRegion(r)
	self.regions[r] = nil
	
	iup.Detach(b)
	iup.Destroy(b)
	if (not teardown) then table.remove(self.buttons, index) end
end

function db:CreateRegions(region_type, init)
	for i, bd in ipairs(self.buttondefs) do
		if (not region_type or bd.type == region_type) then 
			if ((hud_type and bd.type == 'field') or bd.type ~= 'field') then
				self:CreateButton(i, init) -- originally passed buttondef, not sure if I need it to still.
			end
		end
	end
end

function db:DestroyRegions(region_type, teardown)
	--for i, b in pairs(self.buttons or {}) do 
	for i = #self.buttondefs, 1, -1 do
		local b = self.buttons[i]
		if (tonumber(i) and pcall(iup.GetType, b)) then 
			if (not region_type or b.Def.type == region_type) then
				pcall(self.DeleteButton, db, i, teardown)
			end
		end
	end
end

function db:DestroyTouchToTargetRegion()
	if (self.touchtotargetregion) then
		gkinterface.DestroyTouchRegion(self.touchtotargetregion)
		self.regions[self.touchtotargetregion] = nil
		self.touchtotargetregion = nil
	end
end

function db:CreateTouchToTargetRegion()
	self:DestroyTouchToTargetRegion()
	if (self.disable_default_ui and self.enable_touch_to_target) then
		
		local r = gkinterface.CreateTouchRegion(nil, nil, nil, false, false, false, false, false, false, 1, 1, self.xres, self.yres, 1) -- TODO change the lower bounds back to 0 after testing
		self.touchtotargetregion = r
		self.regions[r] = 'target'
	end
end

function db:CreateInflightConfig(button)
	local buttondef = button.Def
	local gap = 10
	local max_len = 0
	for i, t in ipairs(buttondef.commands) do
		local new_len = t.title:len()
		max_len = new_len > max_len and new_len or max_len
	end
	local sizer = iup.stationbutton{title = string.rep("#", max_len + 5), font = Font.H2}
	iup.Map(sizer)

	local box_w = tonumber(sizer.w) -- size:match("(%d+)x"))
	local box_x = (buttondef.x + buttondef.w / 2) - (box_w / 2)
	if (box_x < 0) then box_x = 0 end
	if (box_x + box_w > self.xres) then box_x = self.xres - box_w end 
	local box_h = (tonumber(sizer.h) + gap) * (#buttondef.commands) -- size:match("x(%d+)")) + gap) * (#buttondef.commands)
	local box_y = buttondef.y - box_h
	if (box_y < 0) then box_y = buttondef.y + buttondef.h - 1 + gap end
	
	button.config_button_box = iup.vbox{gap = gap, cx = box_x, cy = box_y}
	for i, c in ipairs(buttondef.commands) do
		iup.Append(button.config_button_box, iup.stationbutton{title = buttondef.commands[i].title, size = box_w.."x", fgcolor = buttondef.commands.current == i and "255 0 0" or nil, font = Font.H2})
	end
	iup.Append(iup.GetNextChild(self.cbox), button.config_button_box)
	iup.Map(self.cbox)
	self.cbox.REFRESH = "YES"
	
	gkinterface.ChangeTouchRegionSize(tonumber(button.Region), 
		box_x < buttondef.x and box_x or buttondef.x,
		box_y < buttondef.y and box_y or buttondef.y, 
		(box_x + box_w - 1), 
		box_y < buttondef.y and (buttondef.y + buttondef.h - 1) or (box_y + box_h - 1)
	)

end

function db:HandleInflightConfig(button, percentW, percentH)
	local buttondef = button.Def
	local gap = 10
	local button_h = tonumber(iup.GetNextChild(button.config_button_box).size:match("x(%d+)"))
	local reg_h = (button_h + gap) * #buttondef.commands + buttondef.h - 1
	for i = 1, #buttondef.commands do
		if (tonumber(button.cy) > tonumber(button.config_button_box.cy)) then
			
			-- If button options are positioned above the button
			if (percentH > ((button_h + gap) * (i - 1)) / reg_h and percentH < (((button_h + gap) * (i - 1)) + button_h) / reg_h) then
				buttondef.commands.current = i
				button.title = buttondef.commands[i].title
				db:save_buttondefs()
				break
			end
		else
			-- If button options are positioned below the button
			if (percentH > (((button_h + gap) * (i - 1)) + button.h + gap) / reg_h and percentH < (((button_h + gap) * (i - 1)) + button_h + button.h + gap) / reg_h) then
				buttondef.commands.current = i
				button.title = buttondef.commands[i].title
				db:save_buttondefs()
				break
			end
			
		end
	end

	iup.Detach(button.config_button_box)
	iup.Destroy(button.config_button_box)
	button.config_button_box = nil
	gkinterface.ChangeTouchRegionSize(tonumber(button.Region), buttondef.x, buttondef.y, (buttondef.x + buttondef.w - 1), (buttondef.y + buttondef.h - 1))

end

function db:on_touch_pressed(id, percentW, percentH)
	--local stime = gkmisc.GetGameTime()
	--console_print(string.format("### PRESS :: Start Region %d", id))
	--console_print(string.format("### id = %d / percentW = %f / percentH = %f", id, percentW, percentH))
	local button = self.regions[id]
	-- An entry is only made in regions if command1 is not a +command
	if (type(button) == "userdata") then
		local buttondef = button.Def
		if (not button.Bound and buttondef.type ~= "field") then 
			if (button.Timer) then -- A timer is not created for short-press +commands.
				--Initiate timer to test for long-press condition.  
				-- If the long-press command is a +command, have it start as soon the timer ends.
				button.Timer:SetTimeout(db.long_press_delay, function()
					-- On a long-press start executing plus-commands right away.  Non-plus-commands are executed on release TODO change behavior?
					if (buttondef.button_type == "standard" and ispluscommand(buttondef.command2)) then
						gkinterface.GKProcessCommand(buttondef.command2)
				                       
					elseif (buttondef.button_type == "configurable" and #buttondef.commands > 1) then
						self:CreateInflightConfig(button)
					end
				end)
			end
		end
	end
	--console_print(string.format("$$$ PRESS :: End Region %d -- %dms", id, gkmisc.GetGameTime() - stime))
end

function db:on_touch_released(id, percentW, percentH)
	--local stime = gkmisc.GetGameTime()
	--console_print(string.format("### RELEASE :: Start Region %d", id))
	--console_print(string.format("### id = %d / percentW = %f / percentH = %f", id, percentW, percentH))
	local button = self.regions[id]

	-- Region is associated with a button
	if (type(button) == "userdata") then
		local buttondef, command = button.Def
		if (not button.Bound and buttondef.type ~= "field") then 
			if (button.Timer and button.Timer:IsActive()) then -- if less than 1 second has passed, execute command1
				command = buttondef.command1 or (buttondef.commands and buttondef.commands[buttondef.commands.current].command)
				button.Timer:Kill()
			elseif (buttondef.button_type == "standard") then
				
				--fall back to command1 if command2 is undefined
				command = buttondef.command2 
				if (command) then
					if (ispluscommand(command)) then command = buttondef.command2cancel end
				else
					command = buttondef.command1 
				end
			elseif (buttondef.button_type == "configurable" and #buttondef.commands > 1) then
				self:HandleInflightConfig(button, percentW, percentH)
			end
			if (command) then gkinterface.GKProcessCommand(command) end
		end

	-- Region is part of the Touch-To-Place interface for buttons
	elseif (type(button) == 'function') then
		button(percentW, percentH)
		
-- 	-- Region is associated with Touch-To-Target
	elseif (button == "target") then
        radar.SetRadarSelectionByPoint(percentW, percentH, self.touch_to_target_command)
    end
	--console_print(string.format("$$$ RELEASE :: End Region %d -- %dms", id, gkmisc.GetGameTime() - stime))
end

function db:OnEvent(event, ...)
	if     (event == 'HUD_SHOW') then
		hud_type = ...
		self:CreateRegions('field')

	elseif (event == 'HUD_HIDE') then
		hud_type = nil
		self:DestroyRegions('field')

	elseif (event == 'TOUCH_PRESSED') then
		self:on_touch_pressed(...)

	elseif (event == 'TOUCH_RELEASED') then
		self:on_touch_released(...)

	elseif (event == 'PLAYER_ENTERED_GAME') then
		self.buttons = {}
		self:set_accel_commands()
		self:load_buttondefs()
		if (not next(self.buttondefs)) then
			local by = tonumber(HUD.addonframe.y)
			local bx = tonumber(HUD.alladdonicons[1].icon.size:match("(%d+)x")) * 2.5
			self.buttondefs = {
				{title = 'db', type = 'button', command1 = 'db', x = bx, y = by, w = self.defaults["button_width"], h = self.defaults["button_height"], font = self.defaults["font"]},
				{title = 'Reload', type = 'button', command1 = 'reload', x = bx + self.defaults["button_width"] + 8, y = by, w = self.defaults["button_width"], h = self.defaults["button_height"], font = self.defaults["font"]},
			}
		end

		if (self.disable_default_ui) then 
			self.remove_default_ui() 
			--HUD:CreateTouchLayer()
		end

		-- Create Buttons and Regions
		self.cbox = iup.cbox{size = db.hudsize, active = "NO"}
		-- Only create non-fields.  Fields will be created at HUD_SHOW based on the hud_type
		self:CreateRegions('button', true)
		iup.Append(iup.GetNextChild(HUD.pluginlayer), self.cbox)
		HUD.pluginlayer:map()

	elseif (event == 'PLAYER_LOGGED_OUT') then
		self:DestroyRegions(nil, true)
		self.buttons = nil
		if (self.cbox) then -- A fresh android install opens the HUD and uses this event upon closing the demo
			iup.Detach(self.cbox)
			iup.Destroy(self.cbox)
			self.cbox = nil
		end
		hud_type = nil
		
	elseif (event == 'UNLOAD_INTERFACE') then
		-- destroy all regions
		for i, b in pairs(self.regions) do 
			if (tonumber(i)) then gkinterface.DestroyTouchRegion(i) end
		end
		--dar(0, 10000)

	elseif (event == "UPDATE_CHARACTER_LIST") then
		if (db.first_run) then
			if (GetNumCharacters() > 1) then
				local msgdlg = msgdlgtemplate1()
				msgdlg:SetMessage(
					"DroidButtons now shares all buttons between characters.\nThe first character you login with will determine what pre-eixsting button set will be used from now on.",
					"OK",
					function(self) HideDialog(iup.GetDialog(self)) end
				)
				PopupDialog(msgdlg, iup.CENTER, iup.CENTER)
			end
			db.first_run = false
			wInt(db.major, "FirstRun", 1)
		end
	end
end

dofile('functions.lua')
dofile('vars.lua')
dofile('ui.lua')

RegisterEvent(db, 'HUD_SHOW')
RegisterEvent(db, 'HUD_HIDE')
RegisterEvent(db, 'TOUCH_PRESSED')
RegisterEvent(db, 'TOUCH_RELEASED')
RegisterEvent(db, 'PLAYER_ENTERED_GAME')
RegisterEvent(db, 'PLAYER_LOGGED_OUT')
RegisterEvent(db, 'UNLOAD_INTERFACE')
RegisterEvent(db, 'UPDATE_CHARACTER_LIST')

RegisterUserCommand('con', function() gkinterface.GKProcessCommand('consoletoggle') end)
RegisterUserCommand('db', function() db.ui.MainDialog:Show() end)
RegisterUserCommand('TopList', function() gkinterface.GKProcessCommand('+TopList') end)

RegisterUserCommand('reload', db.reload)

--[[ ------------------------------------------------------------------------------------
with the creation of the GUI for adding buttons, this documentation isn't necessary for adding
buttons, However I will retain it for informational purposes.
-- --------------------------------------------------------------------------------------

to add a button to the HUD, add a table to the the buttondefs table.  
it should contain the following keys: title, command1, command2[, command2cancel], font, x, y, w, h

command1 should be assigned the string value of the ingame command to run, or a function.
if command1 is a +command, command2 will not be used

command2 should be assigned the string value of the ingame command to run, or a function.
command2 is the long-press command (1 second wait)
if command2 is a +command, assign the inverse of that command to command2cancel  
  (eg. command2="+shoot2", command2cancel="+shoot2 0")

a list of commands can be found at http://www.vendetta-online.com/manual/app_commands.html

the button examples below are configured for optimal placement on my HTC Inspire 4G.
-- ------------------------------------------------------------------------------------]]
