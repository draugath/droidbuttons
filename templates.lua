local function copytable(s)
	assert(type(s) == 'table', 'expected a table')
	local t = {}
	for k, v in pairs(s) do
		if (type(v) == 'table') then
			t[k] = copytable(v)
		else
			t[k] = v
		end
	end
	return t
end
	

local function list2(params)
	assert(type(params) == "table", "bad argument #1 to 'list2' (table expected, got "..type(params)..")")
	-- the Action callback is only applied to the textbox
	params.expand = params.expand or 'NO'
	params.list = params.list or {}
	params.font = params.font or Font.Default
	params.value = params.value or ''
	params.active = params.active or 'YES'

	local dropdown, textbox, zbox
	
	dropdown = iup.list{dropdown = 'YES', size = params.size, font = params.font, expand = params.expand, fgcolor = '0 0 0 0', visible_items = params.visible_items,
		action = function(self, t, i, v) textbox.value = t; iup.SetFocus(textbox); if (params.action) then textbox:action(nil, t) end end
	}
	for i, v in ipairs(params.list) do dropdown[i] = v; if (v == params.value) then dropdown.value = i end; end
	if (not params.size) then 
		iup.Map(dropdown)
		params.size = tonumber(dropdown.size:match("(%d+)x")) - params.font
	else
		params.size = params.size - params.font
	end
	textbox = iup.text{size = params.size, font = params.font, bgcolor = params.bgcolor, expand = params.expand, value = params.value,
		action = function(self, c, after) if (params.action) then return params.action(self, c, after) end end
	}
	zbox = iup.zbox{
		--dropdown must be before textbox within the zbox to create the proper z-order
		dropdown,
		textbox,
		all = 'YES',
		alignment = 'NW',
	}

	function zbox:Activate(b)
		if (b) then
			dropdown.active = "YES"
			textbox.active = "YES"; textbox.bgcolor = params.bgcolor
		else
			dropdown.active = "NO"
			textbox.active = "NO"; textbox.bgcolor = params.bginactive
		end
	end

	function zbox:Clear()
		dropdown.value = 0
		textbox.value = ""
	end
	
	function zbox:GetValue()
		return textbox.value
	end
	
	function zbox:GetWidth()
		return dropdown.w
	end
		
	
	function zbox:SetActionFunc(f)
		assert(type(f) == "function", "bad argument #1 to 'SetAction' (function expected, got "..type(f)..")")
		params.action = f
	end
	
	function zbox:SetText(t)
		textbox.value = tostring(t)
	end
	
	if (params.active:upper() == "NO") then zbox:Activate(false) end
	
	return zbox, textbox, dropdown
end


local function list3(params)
	params = params or {}
	params.control = 'YES'
	
	local controls, items = {}, {}
	local colors, num_colors = {}, 0
	local actioncb, createitemfunc, sortfunc

	local list = iup.stationsublist(params)

	local function create_subdlg(ctrl)
		assert(type(ctrl) == 'userdata' and iup.GetType(ctrl):match('box'), "iup box expected")
		ctrl = ctrl or iup.hbox{}
		local dlg = iup.dialog{
			ctrl,
			border = 'NO', menubox = 'NO', resize = 'NO',-- active = 'NO', visible = "YES"
		}
		dlg.visible = "YES"
		return dlg
	end
	
	
	local function traverse_and_find_names(parent, t)
		t = t or {}
		depth = depth or 1
		local ctrl

		repeat
			ctrl = iup.GetNextChild(parent, ctrl)
			if (ctrl) then
				if (ctrl.name) then t[ctrl.name] = ctrl end
				-- If the parent has a child, then traverse its children
				traverse_and_find_names(ctrl, t)
			end
		until not ctrl
	
		return t
	end


	-- Custom methods
	function list:AddItem(itemtbl, index)
		assert(type(itemtbl) == 'table', 'table expected')
		itemtbl = copytable(itemtbl)
		if (index) then
			assert(type(index) == 'number', 'number expected')
			table.insert(items, index, itemtbl)
		else
			table.insert(items, itemtbl)
		end
	end

	function list:ClearItems()
		items = {}
	end
	
	function list:ClearList()
		self[1] = nil -- why?
		for i, subdlg in ipairs(controls) do
			iup.Detach(subdlg)
			iup.Destroy(subdlg)
			controls[i] = nil
		end
	end
	
	function list:GetItems()
		-- TODO evaluate possible need for copying the table, instead of passing a reference
		return copytable(items)
	end
	
	function list:GetNumItems()
		return #items
	end
	
	function list:GetValues(index, name)
		assert(index == nil or type(index) == 'number', "number or nil expected")
		if (name) then assert(type(name) == 'string', "string expected") end
		
		local subdlg = controls[index]
		if (not subdlg) then return end

		local retval
		if (name and subdlg.names and subdlg.names[name]) then
			retval = subdlg.names[name].value
		else
			retval = {}
			for n, c in pairs(subdlg.names) do
				retval[n] = c.value
			end
		end
		return retval
	end
	
	function list:MoveItemDown(index)
		if (index >= self:GetNumItems()) then return end
		local moving_item = table.remove(items, index)
		table.insert(items, index + 1, moving_item)
		self:PopulateList()
	end
	
	function list:MoveItemUp(index)
		if (index <= 1) then return end
		local moving_item = table.remove(items, index)
		table.insert(items, index - 1, moving_item)
		self:PopulateList()
	end
	
	
	function list:PopulateList()
		assert(createitemfunc, "You need to assign a create item function.")
		self:ClearList()
		
		--if (sortfunc) then table.sort(controls, sortfunc) end

		for i, item in ipairs(items) do
			local subdlg = create_subdlg(createitemfunc(item, i))
			subdlg.names = traverse_and_find_names(subdlg)
			function subdlg:UpdateItem()
				for n, c in pairs(self.names) do
					item[n] = c.GetValue and c:GetValue() or c.value
				end
			end
			subdlg:map()
			table.insert(controls, subdlg)
		end

		for i, subdlg in ipairs(controls) do
			if (num_colors > 0) then subdlg.bgcolor = colors[(i % num_colors) + 1] end
			iup.Append(self, subdlg)
		end

		self:map()

		self[1] = 1 -- why?
	end
	
	function list:RemoveItem(index)
		local retval = table.remove(items, index)
		self:PopulateList()
		return retval
	end

	function list:SetActionCB(func)
		-- Argument should be a function that will be executed after appropriate color changes have been applied
		-- this function should receive one argument, the selected index
		assert(type(func) == 'function', "Function expected")
		actioncb = func
	end
	
	function list:SetCreateItemFunc(func)
		-- Function should receive an argument identifying the item and return a single box that will be put into a sub-dialog template
		assert(type(func) == 'function', "Function expected")
		createitemfunc = func
	end
	
	function list:SetColors(colortbl)
		-- Function should receive a table with numeric indexes. [0] will be used to indicate the color of the selected item.
		assert(type(colortbl) == 'table', "Table expected")
		if (not colortbl[0] and colortbl[1]) then
-- 			-- if no select color, invert the first color
-- 			local mask = bitlib.bnot(255)
-- 			local selcolorparts = {}
-- 			for v in colortbl[1]:gmatch('%S+') do
-- 				local num = tonumber(v)
-- 				if (v) then num = (bitlib.bnot(v) - mask) else num = v end
-- 				table.insert(selcolorparts, num)
-- 			end
-- 			colortbl[0] = table.concat(selcolorparts, ' ')
		end
		num_colors = #colortbl
		colors = colortbl
		printtable(colors)
	end

	function list:SetItems(itemstbl)
		-- Does this need to also clear the list?
		-- TODO evaluate possible need for copying the table, instead of passing a reference
		assert(type(itemstbl) == 'table', 'table expected')
		items = copytable(itemstbl)
	end

	function list:SetSelected(index)
		if (index) then 
			assert(type(index) == 'number', "number expected")
			self:action(nil, index, 1)
		end
		if (self.vo_listsel) then self:action(nil, tonumber(self.vo_listsel), 0) end
		self.vo_listsel = index
	end
	
	function list:SetValues(index, name, values)
		name, values = values and name, values or name
		assert(index == nil or type(index) == 'number', "number or nil expected")
		if (name) then assert(type(name) == 'string', "string expected") end
		assert(type(values) == 'table', 'table expected')
		
		values = values or name
		local subdlg = controls[index]
		if (not subdlg) then return end
		
		if (name and subdlg.names and subdlg.names[name]) then
			subdlg.names[name].value = values
		elseif (subdlg.names) then
			for n, v in pairs(values) do
				if (subdlg.names[n]) then subdlg.names[n].value = v end
			end
		end
	end
	
	
-- 	function list:SetSortFunc(func)
-- 		if (func) then assert(type(func) == 'function') end
-- 		sortfunc = func
-- 	end

	-- Callbacks
	function list:action(text, index, state)
		if (state == 1) then
			if (index ~= tonumber(self.vo_listsel)) then
				if (colors[0]) then controls[index].bgcolor = colors[0] end
				self.selected = index
				if (actioncb) then return actioncb(index) end
			else
				self.selected = nil
			end

		else
			controls[index].bgcolor = colors[(index % num_colors) + 1]

		end
	end
	
	function list:multiselect_cb(value)
	end
	
	function list:edit_cb(c, after)
	end
	
	function list:caret_cb(row, col)
	end
	
	return list
end



-- 3 buttons, save/discard/cancel type 
local function msgdlgtemplate3(msg, button1text, button1action, button2text, button2action, button3text, button3action, dlgbgcolor, alignment)
	local dlg
	local infolabel = iup.label{font=Font.H1,title=msg or "Title", alignment=alignment}
	local button1 = iup.stationbutton{title=button1text or "Save", action=button1action}
	local button2 = iup.stationbutton{title=button2text or "Discard", action=button2action}
	local button3 = iup.stationbutton{title=button3text or "Cancel", action=button3action}

	dlg = iup.dialog{
		iup.hbox{
			iup.fill{},
			iup.vbox{
				iup.fill{},
				iup.stationhighopacityframe{
					bgcolor=dlgbgcolor,
					iup.stationhighopacityframebg{
						bgcolor=dlgbgcolor,
						iup.vbox{
							infolabel,
							iup.hbox{
								iup.fill{},
								button1,
								iup.fill{},
								button2,
								iup.fill{},
								button3,
								iup.fill{},
							},
						},
						expand="NO",
					},
				},
				iup.fill{},
			},
			iup.fill{},
		},
		--defaultenter = button1,
		defaultesc = button3,
		fullscreen="YES",
		bgcolor = "0 0 0 128 *",
		topmost="YES",
	}

	function dlg:SetMessage(msg, button1text, callback1, button2text, callback2, button3text, callback3, alignment)
		infolabel.alignment = alignment or "ALEFT"
		infolabel.title = msg or ""
		button1.title = button1text or "Save"
		if callback1 then button1.action = callback1 end
		button2.title = button2text or "Discard"
		if callback2 then button2.action = callback2 end
		button3.title = button3text or "Cancel"
		if callback3 then button3.action = callback3 end
		dlg.size = nil
	end

	return dlg
end

return list2, list3, msgdlgtemplate3
