DroidButtons.vars = {
	TabState = {
		-- BadValue = <boolean>,
		-- Dirty = <boolean>,
	},
	
	commandlist = { --commandlist
		"+TurnLeft",
		"+TurnRight",
		"+TurnUp",
		"+TurnDown",
		"+StrafeLeft",
		"+StrafeRight",
		"+StrafeUp",
		"+StrafeDown",
		"+RotateCW",
		"+RotateCCW",
		"+Accelerate",
		"+Decelerate",
		"+Turbo",
		"+Brakes",
		"+Shoot2",
		"+Shoot1",
		"+Shoot3",
		"+ptt",
		"+zoom",
	}, 

	commandlist_noplus = { --commandlist
		"Weapon1",
		"Weapon2",
		"Weapon3",
		"Weapon4",
		"Weapon5",
		"Weapon6",
		"RadarNext",
		"RadarPrev",
		"RadarNextFront",
		"RadarPrevFront",
		"RadarNextFrontEnemy",
		"RadarPrevFrontEnemy",
		"RadarNextNearestPowerup",
		"RadarPrevNearestPowerup",
		"RadarNextNearestEnemy",
		"RadarHitBy",
		"RadarNone",
		"TopList", -- custom command with DroidButtons that calls +TopList
		"ConsoleToggle",
		"ViewToggle",
		"CameraToggle",
		"HudToggle",
		"allhudtoggle",
		"HudMode",
		"Activate",
		"FlyModeToggle",
		"MLookToggle",
		"Jettison",
		"ToggleInventory",
		"missionchat",
		"Say_Channel",
		"Say_Group",
		"Say_Guild",
		"Say_Sector",
		"Say_Help",
		"scrollback",
		"scrollforward",
		"ToggleHostiles",
		"ToggleFriendlies",
		"ToggleWormholes",
		"ToggleObjects",
		"ToggleStations",
		"ToggleCargo",
		"ToggleMissiles",
		"ShowAll",
		"explode",
		"Dump",
		"nav",
		"charinfo",
		"toggleautoaim",
		"hudscale",
		"hail",
	}, 

	joystick_cmds = { -- joystick_cmds
		-- cmd name = visual name
		["NONE"] = "none",
		["Turn"] = "Turn",
		["Pitch"] = "Pitch",
		["Roll"] = "Roll",
		["Throttle"] = "Throttle",
		["Accel"] = "Accelerate",
		["AccelTouch"] = "Accelerate (Touch)",
		["StrafeLR"] = "Strafe Left/Right",
		["StrafeUD"] = "Strafe Up/Down",
	},

	joycmdlist3 = { -- joycmdlist3
		-- index = cmd name
		"NONE",
		"Turn",
		"Pitch",
		"Roll",
		"Throttle",
		"Accel",
		"AccelTouch",
		"StrafeLR",
		"StrafeUD",
--		"+LookX",
--		"+LookY",
	},

	joycmdlist = { -- joycmdlist
		"none",
		"Turn",
		"Pitch",
		"Roll",
		"Throttle",
		"Accelerate",
		"Acclerate (Touch)",
		"Strafe Left/Right",
		"Strafe Up/Down",
--		"+LookX",
--		"+LookY",
		dropdown="YES",
		expand="NO",
	},

	radarcmdlist = {
		"RadarNext",
		"RadarPrev",
		"RadarNextFront",
		"RadarPrevFront",
		"RadarNextFrontEnemy",
		"RadarPrevFrontEnemy",
		"RadarNextNearestPowerup",
		"RadarPrevNearestPowerup",
		"RadarNextNearestEnemy",
		dropdown="YES",
		expand="NO",
	},
	
	sensitivity_curve_list = {
							-- filtering / non-filtered +256
		"Linear",           -- 0
		"Cube Root",        -- 1
		"Vector Linear",    -- 2
		"Vector Cube Root", -- 3
		"Vector Gamepad",   -- 4
		dropdown="YES",
		expand="NO",
	},
}

do
	for i, v in ipairs(DroidButtons.vars.commandlist_noplus) do table.insert(DroidButtons.vars.commandlist, v) end
	table.sort(DroidButtons.vars.commandlist_noplus, function(a, b) return a:lower() < b:lower() end)
	table.sort(DroidButtons.vars.commandlist, function(a, b) return a:lower() < b:lower() end)

	local tbl = {}
	for i, v in ipairs(DroidButtons.vars.radarcmdlist) do tbl[v] = i end
	DroidButtons.vars.radarcmdlist2 = tbl

	tbl = {}
	for i, v in ipairs(DroidButtons.vars.joycmdlist3) do tbl[v] = i end
	DroidButtons.vars.joycmdlist2 = tbl
	
	tbl = {}
	for i, v in pairs(DroidButtons.vars.sensitivity_curve_list) do tbl[v] = i end
	DroidButtons.vars.sensitivity_curve_list2 = tbl
end
